package org.horizen.ongoingprojectmonitoring.Root

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import org.horizen.ongoingprojectmonitoring.Helper.LocalDatabase
import org.horizen.ongoingprojectmonitoring.Helper.TinyDB
import java.text.SimpleDateFormat
import java.util.*

open class RootFragment: Fragment() {

    lateinit var prefs: TinyDB
    lateinit var rootVm: RootVm
    lateinit var localDb: LocalDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(activity!!.applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database!!
    }

    fun getDeviceDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.format(Date())
    }

    fun getDeviceTime(): String {
        val sdf = SimpleDateFormat("hh:mm:ss")
        return sdf.format(Date())
    }

}