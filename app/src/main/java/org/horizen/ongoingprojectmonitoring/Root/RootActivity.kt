package org.horizen.ongoingprojectmonitoring.Root

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.util.Log.e
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.snatik.storage.Storage
import android.arch.lifecycle.Observer
import android.widget.Toast
import okhttp3.*
import org.horizen.ongoingprojectmonitoring.Helper.*
import org.horizen.ongoingprojectmonitoring.Main.Home.HomeActivity
import org.horizen.ongoingprojectmonitoring.Models.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.net.URL
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

@SuppressLint("Registered")
open class RootActivity : AppCompatActivity(), ConnectivityReceiver.ConnectivityReceiverListener {
    protected lateinit var fusedLocationClient: FusedLocationProviderClient
    protected lateinit var locationCallback: LocationCallback
    protected val REQUEST_PERMISSION = 0
    protected var mMap: GoogleMap? = null
    protected lateinit var storage: Storage
    protected lateinit var path: String
    protected lateinit var mainPath: String
    protected lateinit var prefs: TinyDB
    protected lateinit var rootVm: RootVm
    protected var localDb: LocalDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database
        storage = Storage(applicationContext)
        path = storage.externalStorageDirectory
        mainPath = path + File.separator + "Ongoing Project Monitoring" + File.separator + "Data"
        val dirExists = storage.isDirectoryExists(mainPath)
        if (!dirExists) {
            storage.createDirectory(mainPath)
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations) {
                    rootVm.currentLocation.value = location
                }
            }
        }

        rootVm.locationPointList?.observe(this@RootActivity, Observer { locations ->
            if (locations != null && locations.isNotEmpty()) {
                val locationArray = ArrayList<LocationPoint>()
                for (location in  locations) {
                    if (!location.status) {
                        locationArray.add(location)
                    }
                }
                doAsync {
                    uploadLocation(locationArray)
                }
            }
        })

        rootVm.imagesList?.observe(this@RootActivity, Observer { images ->
            if (images != null && images.isNotEmpty()) {
                val locationImagesArray = ArrayList<ConstructionSiteImage>()
                val featureImagesArray = ArrayList<ConstructionSiteImage>()

                for (image in  images) {
                    if (!image.status) {
                        if (image.progress_type_id.toInt() == 0) {
                            locationImagesArray.add(image)
                        } else {
                            featureImagesArray.add(image)
                        }
                    }
                }

                doAsync {
                    uploadLocationImage(locationImagesArray)
                    uploadFeatureImage(featureImagesArray)
                }
            }
        })

        rootVm.videoList?.observe(this@RootActivity, Observer { images ->
            if (images != null && images.isNotEmpty()) {
                for (image in  images) {
                    if (!image.status) {
                        doAsync {
                            uploadFeatureVideo(image)
                        }
                    }
                }
            }
        })
    }

    fun login(username: String, password: String) {

        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("uname", username))
                valuePairs.add(ValuePair("password", password))

                e("Login Request", getRequestString("login.php", valuePairs))

                val resp = Server.callApi(Server.serverUrl + "login.php", valuePairs)
                e("Login Response", resp)

                val jobj = JSONObject(resp)
                if (!jobj.getBoolean("status")) {
                    runOnUiThread {
                        rootVm.authstatus.value = 0
                        AlertDialog.Builder(this@RootActivity).setMessage("Wrong Username or Password.").setTitle("Login Failed").setPositiveButton("Close", null).create().show()
                    }
                } else {
                    if (jobj.getString("version") == "1.1.2") {

                        val user = DataTypeConverter().fromStringToUser(resp)
                        user.uname = username
                        user.password = password

                        prefs.putString("user_id", user.user_id)
                        doAsync {
                            localDb?.userDao()?.insertUser(user)
                        }

                        runOnUiThread {
                            rootVm.activeUser.value = user
                            rootVm.authstatus.value = 2
                        }
                    } else {
                        runOnUiThread {
                            rootVm.authstatus.value = 0
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                toast("Login Error" + ex.message)
                runOnUiThread {
                    rootVm.authstatus.value = 0
                }
            }
        }
    }

    protected fun downloadKmlData(value: User) {
        rootVm.actionMessage.value = "Downloading map related data from server for your assigned scheme."
        doAsync {
            try {
                val valuePairs = ArrayList<ValuePair>()
                valuePairs.add(ValuePair("project_id", value.project_id))

                e("Download Kml Request", getRequestString("get-kml-data.php", valuePairs))

                val resp = Server.callApi(Server.serverUrl + "get-kml-data.php", valuePairs)
                e("Download Kml Response", resp)

                val array = JSONArray(resp)
                for (index in 0 until array.length()) {
                    val obj = array[index] as JSONObject
                    val kml = DataTypeConverter().fromStringToKml(obj.toString())
                    kml.project_id = value.project_id
                    localDb?.kmlDao()?.insertKml(kml)
                }

            } catch (ex: Exception) {
                ex.printStackTrace()
                toast("Login Error" + ex.message)
            }
            downloadMouzaData(value)
        }
    }

    protected fun downloadMouzaData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading map related data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Mouza Request", getRequestString("get_location_master.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_location_master.php", valuePairs)
            e("Download Mouza Response", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val mouza = DataTypeConverter().fromStringToMouza(obj.toString())
                mouza.project_id = value.project_id
                localDb?.mouzaDao()?.insertMouza(mouza)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadMaterialData(value)
    }

    protected fun downloadMaterialData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading pipe material data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Material Req", getRequestString("get_material_master.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_material_master.php", valuePairs)
            e("Download Material Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val material = DataTypeConverter().fromStringToMaterial(obj.toString())
                localDb?.materialDao()?.insertMaterial(material)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadClassData(value)
    }

    protected fun downloadClassData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading pipe class data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Class Req", getRequestString("get_class_master.php", valuePairs))

            var resp = Server.callApi(Server.serverUrl + "get_class_master.php", valuePairs)
            e("Download Class Res", resp)

            resp = resp.replace("\"class\":\"", "\"class_\":\"")
            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val class_ = DataTypeConverter().fromStringToClass(obj.toString())
                localDb?.classDao()?.insertClass(class_)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadTypeData(value)
    }

    protected fun downloadTypeData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading pipe type data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("session_key", value.project_id))

            e("Download Type Req", getRequestString("get_type.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_type.php", valuePairs)
            e("Download Type Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val type = DataTypeConverter().fromStringToType(obj.toString())
                localDb?.typeDao()?.insertType(type)
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadDiameterData(value)
    }

    protected fun downloadDiameterData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading pipe diameter data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("session_key", value.project_id))

            e("Download Diameter Req", getRequestString("get_dia_master.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_dia_master.php", valuePairs)
            e("Download Diameter Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array.getJSONObject(index)
                val stringVal = obj.getString("diameter")
                val diameterSplit = stringVal.split(",")
                for (dia in diameterSplit) {
                    val type = DataTypeConverter().fromStringToDiameter(obj.toString())
                    type.diameter = dia
                    type.serial = type.serial + type.diameter
                    localDb?.diameterDao()?.insertDiameter(type)
                }
            }

        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadConstructionSiteData(value)
    }

    protected fun downloadConstructionSiteData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading construction site data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("session_key", value.project_id))
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Site Req", getRequestString("get_construction_site.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_construction_site.php", valuePairs)
            e("Download Site Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val type = DataTypeConverter().fromStringToConstructionSite(obj.toString())
                localDb?.constructionSite()?.insertConstructionSite(type)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }
        downloadAgencyData(value)
    }

    protected fun downloadAgencyData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading agency data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("session_key", value.project_id))
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Agency Req", getRequestString("get_agency.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_agency.php", valuePairs)
            e("Download Agency Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val agency = DataTypeConverter().fromStringToAgency(obj.toString())
                localDb?.agencyDao()?.insertAgency(agency)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Agency Error" + ex.message)
        }
        downloadProgressHead(value)
    }

    protected fun downloadProgressHead(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading progress head status data from server for your assigned scheme."
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("session_key", value.project_id))
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Progress Req", getRequestString("get_progress_head.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get_progress_head.php", valuePairs)
            e("Download Progress Res", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val agency = DataTypeConverter().fromStringToProgressHead(obj.toString())
                localDb?.progressHeadDao()?.insertProgressHead(agency)
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Agency Error" + ex.message)
        }
        downloadPointData(value)
    }

    private fun downloadPointData(value: User) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading point data"
        }
        try {
            val valuePairs = ArrayList<ValuePair>()
            valuePairs.add(ValuePair("project_id", value.project_id))

            e("Download Point Request", getRequestString("get-point-data.php", valuePairs))

            val resp = Server.callApi(Server.serverUrl + "get-point-data.php", valuePairs)
            e("Download Point Response", resp)

            val array = JSONArray(resp)
            for (index in 0 until array.length()) {
                val obj = array[index] as JSONObject
                val point = DataTypeConverter().fromStringToPoint(obj.toString())
                point.project_id = value.project_id
                localDb?.pointDao()?.insertPoint(point)
            }


        } catch (ex: Exception) {
            ex.printStackTrace()
            toast("Login Error" + ex.message)
        }

        val dataList = localDb?.kmlDao()?.getAllKmls()
        if (dataList != null && dataList.isNotEmpty()) {
            for (i in 0 until dataList.size) {
                val data = dataList[i]
                downloadSettlementTask(data)
            }
        }
    }

    private fun downloadSettlementTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading settlement data"
        }
        val fileName = "settlement.kml"
        try {
            val inputStream = URL(data.settlement).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Settlement", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }
        downloadPrimaryRisingMain(data)
    }

    private fun downloadPrimaryRisingMain(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading primary rising main data"
        }
        val fileName = "primary_rising_main.kml"
        try {
            val inputStream = URL(data.primery_rising_main).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("P Rising Main", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadSecondaryRisingMain(data)
    }

    private fun downloadSecondaryRisingMain(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading secondary rising main data"
        }
        val fileName = "secondary_rising_main.kml"
        try {
            val inputStream = URL(data.secondary_rising_main).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("S Rising Main", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }
        downloadBlockTask(data)
    }

    private fun downloadBlockTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading block data"
        }
        val fileName = "block.kml"
        try {
            val inputStream = URL(data.block_boundary).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Block", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadPanchayatTask(data)
    }

    private fun downloadPanchayatTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading panchayat data"
        }
        val fileName = "panchayat.kml"
        try {
            val inputStream = URL(data.gp_boundary).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Gp", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadMouzaTask(data)
    }

    private fun downloadMouzaTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading mouza data"
        }
        val fileName = "mouza.kml"
        try {
            val inputStream = URL(data.mouza_boundary).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Mouza", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadZoneTask(data)
    }

    private fun downloadZoneTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading zone data"
        }
        val fileName = "zone.kml"
        try {
            val inputStream = URL(data.zone_boundary).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Zone", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadWaterBodyTask(data)
    }

    private fun downloadWaterBodyTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading water body data"
        }
        val fileName = "water_body.kml"
        try {
            val inputStream = URL(data.water_body).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Water Body", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadCommunicationTask(data)
    }

    private fun downloadCommunicationTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading communication data"
        }
        val fileName = "road.kml"
        try {
            val inputStream = URL(data.communication).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Communication", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        downloadSchemeTask(data)
    }

    private fun downloadSchemeTask(data: Kml) {
        runOnUiThread {
            rootVm.actionMessage.value = "Downloading scheme data"
        }

        val fileName = "scheme.kml"
        try {
            val inputStream = URL(data.project_boundary).openStream()
            val settlePath = mainPath + "/" + data.project_id
            Log.e("Scheme", settlePath)
            val dirExists = storage.isDirectoryExists(settlePath)
            if (!dirExists) {
                storage.createDirectory(settlePath)
            }
            inputStream.use { input ->
                File("$settlePath/$fileName").outputStream().use { fileOut ->
                    input.copyTo(fileOut)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.e("Error", e.message)
        }

        runOnUiThread {
            prefs.putBoolean(Defaults.LOGGED_IN, true)
            startActivity(Intent(this@RootActivity, HomeActivity::class.java))
        }
    }

    fun getRequestString(api: String, valuePairs: ArrayList<ValuePair>): String {
        var request = "${Server.serverUrl}$api?"
        for (valuePair in valuePairs) {
            request += "${valuePair.name}=${valuePair.value}&&"
        }
        return request
    }

    val locationRequest = LocationRequest().apply {
        interval = 5000
        fastestInterval = 2000
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    @SuppressLint("MissingPermission")
    fun getLastLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                if (mMap != null) {
                    val cameraUpdate = CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(LatLng(location.latitude, location.longitude), 20f))
                    mMap!!.animateCamera(cameraUpdate)
                }
            }
        }
    }

    fun uploadFeatureImage(imageList: ArrayList<ConstructionSiteImage>) {
        for (image in imageList) {
            e("Feature Image Data", "http://maps.wbphed.gov.in/ongoing_project_monitoring/mobile_service/location_file_upload.php?session_key=000&&" +
                    "user_id=${image.user_id}&&location_id=${image.point_id}&&project_id=${image.user_id}&&gps_lat=${image.gps_lat}&&gps_lon=${image.gps_lon}&&" +
                    "image_caption=${image.image_captionn}&&progress_type_id=0&&file_path=${image.file_path}")

            try {
                val path = image.file_path
                val filename = path.substring(path.lastIndexOf("/") + 1)
                val client = OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.MINUTES).writeTimeout(30, TimeUnit.MINUTES).build()
                val requestBody = MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("point_id", image.point_id)
                        .addFormDataPart("user_id", image.user_id)
                        .addFormDataPart("project_id", image.project_id)
                        .addFormDataPart("gps_lat", image.gps_lat)
                        .addFormDataPart("gps_lon", image.gps_lon)
                        .addFormDataPart("image_caption", image.image_captionn)
                        .addFormDataPart("file", filename,
                                RequestBody.create(MediaType.parse("image/jpg"), File(path)))

                val request = Request.Builder()
                        .url(Server.serverUrl + "feature_file_upload.php")
                        .post(requestBody.build())
                        .build()
                val resp = client.newCall(request).execute()
                val s = resp.body()!!.string()

                // [{"status":"success1","record_id":451}]
                Log.e("resp", s)
                val array = JSONArray(s)
                for (index in 0 until array.length()) {
                    val obj = array.getJSONObject(index)
                    val status = obj.getString("status")
                    if (status.contains("success")) {
                        image.status = true
                        localDb?.constructionSiteImageDao()?.updateConstructionSiteImage(image)
                    } else {
                        e("Failed", "Image Upload Failed")
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                if (e.message != null) {
                    Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    fun uploadFeatureVideo(image: ConstructionSiteVideo) {
        e("Location Image Data","http://maps.wbphed.gov.in/ongoing_project_monitoring/mobile_service/location_file_upload.php?session_key=000&&" +
                "user_id=${image.user_id}&&location_id=${image.point_id}&&project_id=${image.user_id}&&gps_lat=${image.gps_lat}&&gps_lon=${image.gps_lon}&&" +
                "image_caption=${image.image_caption}&&progress_type_id=0&&file_path=${image.file_path}")

        try {
            val path = image.file_path
            val filename = path.substring(path.lastIndexOf("/") + 1)
            val client = OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.MINUTES).writeTimeout(30, TimeUnit.MINUTES).build()
            val requestBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("point_id", image.point_id)
                    .addFormDataPart("user_id", image.user_id)
                    .addFormDataPart("project_id", image.project_id)
                    .addFormDataPart("gps_lat", image.gps_lat)
                    .addFormDataPart("gps_lon", image.gps_lon)
                    .addFormDataPart("image_caption", image.image_caption)
                    .addFormDataPart("file", filename,
                            RequestBody.create(MediaType.parse("*/*"), File(path)))

            val request = Request.Builder()
                    .url(Server.serverUrl + "upload_video.php")
                    .post(requestBody.build())
                    .build()
            val resp = client.newCall(request).execute()
            val s = resp.body()!!.string()
            Log.e("resp", s)
            val array = JSONArray(s)
            for(index in 0 until array.length()) {
                val obj = array.getJSONObject(index)
                val status = obj.getString("status")
                if (status.contains("success")) {
                    image.status = true
                    localDb?.constructionSiteVideoDao()?.updateConstructionSiteVideo(image)
                } else {
                    e("Failed", "Image Upload Failed")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
            if (e.message != null) {
                Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
            }
        }
    }

    fun uploadLocation(currentPointDataList: ArrayList<LocationPoint>) {
        for (currentPointData in currentPointDataList) {
            val valuePairs = DataTypeConverter().fromObjectToArrayList(currentPointData)
            valuePairs.add(ValuePair("class", currentPointData.class_))
            try {
                e("Upload Data Request", getRequestString("upload_location_data.php", valuePairs))
                val resp = Server.callApi(Server.serverUrl + "upload_location_data.php", valuePairs)
                e("Upload Data Response", resp)
                val jArray = JSONArray(resp)
                for (index in 0 until jArray.length()) {
                    val obj = jArray.getJSONObject(index)
                    val recId = obj.getString("record_id")
                    if (obj.getBoolean("status")) {
                        val currentImageData = localDb?.constructionSiteImageDao()?.getAllConstructionSiteImagesById(currentPointData.id)

                        currentPointData.status = true
                        localDb?.locationPointDao()?.updateLocationPoint(currentPointData)

                        currentImageData?.point_id = recId
                        currentImageData?.status = false

                        e("Image Id", currentImageData?.point_id)
                        e("Point Id", recId)

                        if (currentImageData != null) {
                            localDb?.constructionSiteImageDao()?.updateConstructionSiteImage(currentImageData)
                        }
                    }
                }
            } catch (ex: Exception) {
                ex.printStackTrace()
                toast("Data Upload Error" + ex.message)
            }
        }
    }

    fun uploadLocationImage(imageList: ArrayList<ConstructionSiteImage>) {
        for (image in imageList) {
            e("Location Image Data", "http://maps.wbphed.gov.in/ongoing_project_monitoring/mobile_service/location_file_upload.php?session_key=81dc9bdb52d04dc20036dbd8313ed055&&" +
                    "user_id=${image.user_id}&&location_id=${image.point_id}&&project_id=${image.user_id}&&gps_lat=${image.gps_lat}&&gps_lon=${image.gps_lon}&&" +
                    "image_caption=${image.image_captionn}&&progress_type_id=0&&file_path=${image.file_path}")
            try {
                val path = image.file_path
                val filename = path.substring(path.lastIndexOf("/") + 1)
                val client = OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.MINUTES).writeTimeout(30, TimeUnit.MINUTES).build()
                val requestBody = MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("location_id", image.point_id)
                        .addFormDataPart("user_id", image.user_id)
                        .addFormDataPart("project_id", image.project_id)
                        .addFormDataPart("gps_lat", image.gps_lat)
                        .addFormDataPart("gps_lon", image.gps_lon)
                        .addFormDataPart("image_caption", image.image_captionn)
                        .addFormDataPart("progress_type_id", "0")
                        .addFormDataPart("file", filename,
                                RequestBody.create(MediaType.parse("image/jpg"), File(path)))

                val request = Request.Builder()
                        .url(Server.serverUrl + "location_file_upload.php")
                        .post(requestBody.build())
                        .build()
                val resp = client.newCall(request).execute()
                val s = resp.body()!!.string()

                // [{"status":"success1","record_id":451}]
                Log.e("resp", s)
                val array = JSONArray(s)
                for (index in 0 until array.length()) {
                    val obj = array.getJSONObject(index)
                    val status = obj.getString("status")
                    if (status.contains("success")) {
                        image.status = true
                        localDb?.constructionSiteImageDao()?.updateConstructionSiteImage(image)
                    } else {
                        e("Failed", "Image Upload Failed")
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                if (e.message != null) {
                    Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private var dialog: ProgressDialog? = null
    @SuppressLint("MissingPermission")
    fun saveFeatureVideo(locationPoint: ConstructionSiteVideo) {
        fusedLocationClient.lastLocation.addOnSuccessListener { location: Location? ->
            if (location != null) {
                doAsync {
                    locationPoint.gps_lat = location.latitude.toString()
                    locationPoint.gps_lon = location.longitude.toString()
                    localDb?.constructionSiteVideoDao()?.insertConstructionSiteVideo(locationPoint)
                }
            } else {
                toast("Unable to get location! Try again.")
            }
        }.addOnFailureListener { exception ->
            toast("Unable to get location! Try again. ${exception.message}")
        }
    }

    @SuppressLint("MissingPermission")
    fun saveFeatureImage(locationPoint: ConstructionSiteImage) {
        doAsync {
            localDb?.constructionSiteImageDao()?.insertConstructionSiteImage(locationPoint)
        }
    }

    protected var connected = false

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        setNetworkStatus(isConnected)
    }

    override fun onResume() {
        super.onResume()
        RootApplication.instance!!.setConnectivityListener(this)
    }

    fun checkConnection() {
        val isConnected = ConnectivityReceiver.isConnected
        setNetworkStatus(isConnected)
    }

    private fun setNetworkStatus(isConnected: Boolean) {
        connected = isConnected
        if (connected) {
            toast("Internet connected!")

        } else {
            toast("Internet is not available!")
        }
    }
}

