package org.horizen.ongoingprojectmonitoring.Root

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import com.snatik.storage.Storage
import org.horizen.ongoingprojectmonitoring.Helper.LocalDatabase
import org.horizen.ongoingprojectmonitoring.Helper.TinyDB
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

open class RootDialogFragment: DialogFragment() {

    lateinit var prefs: TinyDB
    lateinit var rootVm: RootVm
    lateinit var localDb: LocalDatabase
    protected lateinit var storage: Storage
    protected lateinit var path: String
    protected lateinit var mainPath: String
    protected val REQUEST_VIDEO_CAPTURE = 1887

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        prefs = TinyDB(activity!!.applicationContext)
        rootVm = ViewModelProviders.of(this).get(RootVm::class.java)
        localDb = RootApplication.database!!
        storage = Storage(activity!!.applicationContext)
        path = storage.externalStorageDirectory
        mainPath = path + File.separator + "Ongoing Project Monitoring" + File.separator + "Data"
        val dirExists = storage.isDirectoryExists(mainPath)
        if (!dirExists) {
            storage.createDirectory(mainPath)
        }
    }

    fun getDeviceDate(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        return sdf.format(Date())
    }

    fun getDeviceTime(): String {
        val sdf = SimpleDateFormat("hh:mm:ss")
        return sdf.format(Date())
    }
}