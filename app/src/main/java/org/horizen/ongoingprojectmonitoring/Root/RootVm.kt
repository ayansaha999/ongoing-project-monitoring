package org.horizen.ongoingprojectmonitoring.Root

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.location.Location
import android.view.View
import org.horizen.ongoingprojectmonitoring.Models.*
import java.text.SimpleDateFormat
import java.util.*

class RootVm: ViewModel() {

    var passwordVisible = MutableLiveData<Boolean>()
    var authstatus = MutableLiveData<Int>()
    var actionMessage = MutableLiveData<String>()

    var currentLocation = MutableLiveData<Location>()
    var habFilterOpen = MutableLiveData<Boolean>()

    var activeUser = MutableLiveData<User>()

    var locationPointList: LiveData<List<LocationPoint>>? = null
    var pointList: LiveData<List<Point>>? = null
    var imagesList: LiveData<List<ConstructionSiteImage>>? = null
    var videoList: LiveData<List<ConstructionSiteVideo>>? = null

    var activeCheckox = MutableLiveData<String>()

    var uploadStatusSummary = MutableLiveData<String>()

    init {
        authstatus.value = 0
        actionMessage.value = ""
        habFilterOpen.value = true
        passwordVisible.value = false
        activeCheckox.value = ""
        uploadStatusSummary.value = ""
        locationPointList = RootApplication.database?.locationPointDao()?.getAllLocationPoints()
        imagesList = RootApplication.database?.constructionSiteImageDao()?.getAllConstructionSiteImages()
        videoList = RootApplication.database?.constructionSiteVideoDao()?.getAllConstructionSiteVideos()
        pointList = RootApplication.database?.pointDao()?.getAllPoints()
    }

    fun setUploadStatusSummary() {
        var leftLocationData = 0
        if (locationPointList != null && locationPointList!!.value != null) {
            for (point in locationPointList!!.value!!) {
                if (!point.status) {
                    leftLocationData += 1
                }
            }
        }

        var leftConstructionSiteImage = 0

        if (imagesList != null && imagesList!!.value != null) {
            for (point in imagesList!!.value!!) {
                if (!point.status) {
                    leftConstructionSiteImage += 1
                }
            }
        }

        var leftConstructionSiteVideo = 0

        if (videoList != null && videoList!!.value != null) {
            for (point in videoList!!.value!!) {
                if (!point.status) {
                    leftConstructionSiteVideo += 1
                }
            }
        }

        uploadStatusSummary.value = if (leftLocationData == 0 && leftConstructionSiteImage == 0 && leftConstructionSiteVideo == 0) {
            "Nothing left to be uploaded!"
        } else if (leftConstructionSiteImage == 0 && leftConstructionSiteVideo == 0) {
            "$leftLocationData/${locationPointList!!.value!!.size} pipeline laying location data are left to be uploaded."
        } else if (leftConstructionSiteVideo == 0 && leftLocationData == 0) {
            "$leftConstructionSiteImage/${imagesList!!.value!!.size} construction site images are left to be uploaded."
        } else if (leftConstructionSiteImage == 0 && leftLocationData == 0) {
            "$leftConstructionSiteVideo/${videoList!!.value!!.size} construction site videos data are left to be uploaded."
        } else if (leftConstructionSiteImage == 0) {
            "$leftLocationData/${locationPointList!!.value!!.size} pipeline laying location data and $leftConstructionSiteVideo/${videoList!!.value!!.size} construction site videos data are left to be uploaded."
        } else if (leftConstructionSiteVideo == 0) {
            "$leftLocationData/${locationPointList!!.value!!.size} pipeline laying location data and $leftConstructionSiteImage/${imagesList!!.value!!.size} construction site images are left to be uploaded."
        } else if (leftLocationData == 0) {
            "$leftConstructionSiteImage/${imagesList!!.value!!.size} construction site images and $leftConstructionSiteVideo/${videoList!!.value!!.size} construction site videos data are left to be uploaded."
        } else {
            "$leftLocationData/${locationPointList!!.value!!.size} pipeline laying location data, $leftConstructionSiteImage/${imagesList!!.value!!.size} construction site images are left to be uploaded and " +
                    "$leftConstructionSiteVideo/${videoList!!.value!!.size} construction site videos data are left to be uploaded."
        }
    }
}