package org.horizen.ongoingprojectmonitoring.Root

import android.app.Application
import android.arch.persistence.room.Room
import android.support.multidex.MultiDex
import org.horizen.ongoingprojectmonitoring.Helper.ConnectivityReceiver
import org.horizen.ongoingprojectmonitoring.Helper.LocalDatabase

class RootApplication: Application() {

    companion object {
        var database: LocalDatabase? = null
        var instance : RootApplication? = null
    }

    override fun onCreate() {
        super.onCreate()
        RootApplication.instance = this
        MultiDex.install(this)
        RootApplication.database = Room.databaseBuilder(this, LocalDatabase::class.java, "ongoing-project-monitoring-db").fallbackToDestructiveMigration().build()
    }

    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }
}