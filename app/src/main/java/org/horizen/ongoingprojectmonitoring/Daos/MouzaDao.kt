package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Mouza

@Dao
interface MouzaDao {

    @Query("select * from Mouza")
    fun getAllMouzas(): List<Mouza>

    @Query("select * from Mouza where project_id = :id")
    fun findMouzaById(id: String): Mouza

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMouza(vararg mouza: Mouza)

    @Update
    fun updateMouza(vararg mouza: Mouza)

    @Delete
    fun deleteMouza(mouza: Mouza)
}