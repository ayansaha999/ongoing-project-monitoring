package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteVideo

@Dao
interface ConstructionSiteVideoDao {
    @Query("select * from ConstructionSiteVideo")
    fun getAllConstructionSiteVideos(): LiveData<List<ConstructionSiteVideo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertConstructionSiteVideo (vararg point : ConstructionSiteVideo)

    @Update
    fun updateConstructionSiteVideo (vararg point : ConstructionSiteVideo)

    @Delete
    fun deleteConstructionSiteVideo (point : ConstructionSiteVideo)
}