package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Agency

@Dao
interface AgencyDao {
    @Query("select * from Agency")
    fun getAllAgencys(): List<Agency>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAgency (vararg point : Agency)

    @Update
    fun updateAgency (vararg point : Agency)

    @Delete
    fun deleteAgency (point : Agency)
}