package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Type 

@Dao
interface TypeDao {
    @Query("select * from Type")
    fun getAllTypes(): List<Type>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertType (vararg point : Type)

    @Update
    fun updateType (vararg point : Type)

    @Delete
    fun deleteType (point : Type)
}