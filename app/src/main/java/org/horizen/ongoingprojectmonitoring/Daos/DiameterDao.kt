package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Diameter 

@Dao
interface DiameterDao {
    @Query("select * from Diameter")
    fun getAllDiameters(): List<Diameter>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDiameter (vararg point : Diameter)

    @Update
    fun updateDiameter (vararg point : Diameter)

    @Delete
    fun deleteDiameter (point : Diameter)
}