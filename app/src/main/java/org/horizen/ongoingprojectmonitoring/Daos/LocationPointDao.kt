package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.LocationPoint

@Dao
interface LocationPointDao {
    
    @Query("select * from LocationPoint")
    fun getAllLocationPoints(): LiveData<List<LocationPoint>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocationPoint (vararg point : LocationPoint)

    @Update
    fun updateLocationPoint (vararg point : LocationPoint)

    @Delete
    fun deleteLocationPoint (point : LocationPoint)
}