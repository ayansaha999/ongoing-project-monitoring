package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.User

@Dao
interface UserDao {
    @Query("select * from User order by uname")
    fun getAllUsers(): List<User>

    @Query("select * from User where uname = :id")
    fun findUserById(id: String): User

    @Query("select * from User where status = :status")
    fun findLoggedInUser(status: Boolean): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(vararg User: User)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateUser(vararg User: User)

    @Delete
    fun deleteUser(User: User)

}