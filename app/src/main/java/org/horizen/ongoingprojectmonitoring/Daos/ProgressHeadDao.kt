package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.ProgressHead

@Dao
interface ProgressHeadDao {
    @Query("select * from ProgressHead")
    fun getAllProgressHeads(): List<ProgressHead>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertProgressHead(vararg point : ProgressHead)

    @Update
    fun updateProgressHead(vararg point : ProgressHead)

    @Delete
    fun deleteProgressHead(point : ProgressHead)
}