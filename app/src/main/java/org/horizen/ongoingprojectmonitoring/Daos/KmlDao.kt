package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Kml

@Dao
interface KmlDao {
    @Query("select * from Kml")
    fun getAllKmls(): List<Kml>

    @Query("select * from Kml where project_id = :id")
    fun findKmlById(id: String): Kml

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertKml(vararg kml: Kml)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updatekml(vararg kml: Kml)

    @Delete
    fun deleteKml(kml: Kml)
}