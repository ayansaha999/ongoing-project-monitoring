package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Class 

@Dao
interface ClassDao {
    @Query("select * from Class")
    fun getAllClasss(): List<Class>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertClass (vararg point : Class)

    @Update
    fun updateClass (vararg point : Class)

    @Delete
    fun deleteClass (point : Class)
}