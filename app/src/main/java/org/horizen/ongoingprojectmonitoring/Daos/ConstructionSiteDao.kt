package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSite

@Dao
interface ConstructionSiteDao {
    @Query("select * from ConstructionSite")
    fun getAllConstructionSites(): List<ConstructionSite>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertConstructionSite (vararg point : ConstructionSite)

    @Update
    fun updateConstructionSite (vararg point : ConstructionSite)

    @Delete
    fun deleteConstructionSite (point : ConstructionSite)
}