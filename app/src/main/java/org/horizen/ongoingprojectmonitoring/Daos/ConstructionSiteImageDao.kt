package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteImage

@Dao
interface ConstructionSiteImageDao {
    @Query("select * from ConstructionSiteImage")
    fun getAllConstructionSiteImages(): LiveData<List<ConstructionSiteImage>>

    @Query("select * from ConstructionSiteImage where id = :id")
    fun getAllConstructionSiteImagesById(id: String): ConstructionSiteImage

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertConstructionSiteImage (vararg point : ConstructionSiteImage)

    @Update
    fun updateConstructionSiteImage (vararg point : ConstructionSiteImage)

    @Delete
    fun deleteConstructionSiteImage (point : ConstructionSiteImage)
}