package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Material 

@Dao
interface MaterialDao {
    @Query("select * from Material")
    fun getAllMaterials(): List<Material>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMaterial (vararg point : Material)

    @Update
    fun updateMaterial (vararg point : Material)

    @Delete
    fun deleteMaterial (point : Material)
}