package org.horizen.ongoingprojectmonitoring.Daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import org.horizen.ongoingprojectmonitoring.Models.Point 

@Dao
interface PointDao {
    @Query("select * from Point")
    fun getAllPoints(): LiveData<List<Point>>

    @Query("select * from Point where project_id = :id")
    fun findPointByScheme(id: String): List<Point>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertPoint (vararg point : Point)

    @Update
    fun updatePoint (vararg point : Point)

    @Delete
    fun deletePoint (point : Point)
}