package org.horizen.ongoingprojectmonitoring.Helper

import android.content.Context
import android.database.Cursor
import android.graphics.*
import android.location.LocationManager
import android.media.ExifInterface

import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import android.opengl.ETC1.getHeight
import android.text.StaticLayout
import android.graphics.Color.parseColor
import android.net.Uri
import android.provider.MediaStore
import android.text.Layout
import android.text.TextPaint



object Util {

    val serverLoc = "http://maps.wbphed.gov.in/water_conservation/webservice/"

    fun isGPSOn(context: Context): Boolean {
        val manager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun resizeImage(path: String, resultPath: String) {
        var scaledBitmap: Bitmap? = null
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        var bmp = BitmapFactory.decodeFile(path, options)
        var actualHeight = options.outHeight
        var actualWidth = options.outWidth
        val maxHeight = 900.0f
        val maxWidth = 900.0f
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (actualHeight > actualWidth) {
                actualWidth = (actualWidth.toFloat() / actualHeight.toFloat() * maxHeight).toInt()
                actualHeight = maxHeight.toInt()
            } else {
                actualHeight = (actualHeight.toFloat() / actualWidth.toFloat() * maxWidth).toInt()
                actualWidth = maxWidth.toInt()
            }
        }

        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight)
        options.inJustDecodeBounds = false
        options.inPurgeable = true
        options.inInputShareable = true
        options.inTempStorage = ByteArray(16 * 1024)

        try {
            bmp = BitmapFactory.decodeFile(path, options)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()

        }

        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888)
        } catch (exception: OutOfMemoryError) {
            exception.printStackTrace()
        }

        val ratioX = actualWidth / options.outWidth.toFloat()
        val ratioY = actualHeight / options.outHeight.toFloat()
        val middleX = actualWidth / 2.0f
        val middleY = actualHeight / 2.0f

        val scaleMatrix = Matrix()
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY)

        val canvas = Canvas(scaledBitmap!!)
        canvas.matrix = scaleMatrix
        canvas.drawBitmap(bmp, middleX - bmp.width / 2, middleY - bmp.height / 2, Paint(Paint.FILTER_BITMAP_FLAG))

        val matrix = Matrix()
        matrix.postRotate(90f)
        var out: FileOutputStream? = null
        try {
            out = FileOutputStream(resultPath)
            try {
                val ei = ExifInterface(path)
                val orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                        ExifInterface.ORIENTATION_UNDEFINED)

                when (orientation) {
                    ExifInterface.ORIENTATION_ROTATE_90 -> scaledBitmap = rotateImage(scaledBitmap, 90f)

                    ExifInterface.ORIENTATION_ROTATE_180 -> scaledBitmap = rotateImage(scaledBitmap, 180f)

                    ExifInterface.ORIENTATION_ROTATE_270 -> scaledBitmap = rotateImage(scaledBitmap, 270f)

                    ExifInterface.ORIENTATION_NORMAL -> {
                    }

                    else -> {
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

            scaledBitmap!!.compress(Bitmap.CompressFormat.JPEG, 100, out)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

    }

    fun rotateImage(source: Bitmap, angle: Float): Bitmap {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(source, 0, 0, source.width, source.height,
                matrix, true)
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            val heightRatio = Math.round(height.toFloat() / reqHeight.toFloat())
            val widthRatio = Math.round(width.toFloat() / reqWidth.toFloat())
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }
        val totalPixels = (width * height).toFloat()
        val totalReqPixelsCap = (reqWidth * reqHeight * 2).toFloat()
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++
        }

        return inSampleSize
    }

    fun drawText(text: String, textWidth: Int, color: Int): Bitmap {
        val textPaint = TextPaint(Paint.ANTI_ALIAS_FLAG or Paint.LINEAR_TEXT_FLAG)
        textPaint.style = Paint.Style.FILL
        textPaint.color = Color.parseColor("#ffffff")
        textPaint.textSize = 30f

        val mTextLayout = StaticLayout(text, textPaint, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false)
        val b = Bitmap.createBitmap(textWidth, mTextLayout.height, Bitmap.Config.ARGB_4444)
        val c = Canvas(b)
        val paint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.LINEAR_TEXT_FLAG)
        paint.style = Paint.Style.FILL
        paint.color = color
        c.drawPaint(paint)
        c.save()
        c.translate(0f, 0f)
        mTextLayout.draw(c)
        c.restore()

        return b
    }

    fun getRealPathFromURI(context: Context, contentUri: Uri): String {
        var cursor: Cursor? = null
        try {
            val proj = arrayOf(MediaStore.Images.Media.DATA)
            cursor = context.contentResolver.query(contentUri, proj, null, null, null)
            val column_index = cursor!!.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            return cursor.getString(column_index)
        } finally {
            cursor?.close()
        }
    }
}
