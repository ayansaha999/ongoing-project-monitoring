package org.horizen.ongoingprojectmonitoring.Helper

import android.arch.persistence.room.TypeConverter

import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import org.horizen.ongoingprojectmonitoring.Models.*
import org.json.JSONException
import org.json.JSONObject
import java.util.*

class DataTypeConverter {

    @TypeConverter
    fun fromConstructionSiteImageToString(data: ConstructionSiteImage): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromConstructionSiteVideoToString(data: ConstructionSiteVideo): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    @TypeConverter
    fun fromStringToAgency(data: String): Agency {
        val dataType = object : TypeToken<Agency>() {}.type
        return Gson().fromJson<Agency>(data, dataType)
    }

    @TypeConverter
    fun fromStringToProgressHead(data: String): ProgressHead {
        val dataType = object : TypeToken<ProgressHead>() {}.type
        return Gson().fromJson<ProgressHead>(data, dataType)
    }

    @TypeConverter
    fun fromStringToType(data: String): Type {
        val dataType = object : TypeToken<Type>() {}.type
        return Gson().fromJson<Type>(data, dataType)
    }

    @TypeConverter
    fun fromStringToConstructionSite(data: String): ConstructionSite {
        val dataType = object : TypeToken<ConstructionSite>() {}.type
        return Gson().fromJson<ConstructionSite>(data, dataType)
    }


    @TypeConverter
    fun fromStringToDiameter(data: String): Diameter {
        val dataType = object : TypeToken<Diameter>() {}.type
        return Gson().fromJson<Diameter>(data, dataType)
    }

    @TypeConverter
    fun fromStringToMouza(data: String): Mouza {
        val dataType = object : TypeToken<Mouza>() {}.type
        return Gson().fromJson<Mouza>(data, dataType)
    }

    @TypeConverter
    fun fromStringToClass(data: String): Class {
        val dataType = object : TypeToken<Class>() {}.type
        return Gson().fromJson<Class>(data, dataType)
    }

    @TypeConverter
    fun fromStringToMaterial(data: String): Material {
        val dataType = object : TypeToken<Material>() {}.type
        return Gson().fromJson<Material>(data, dataType)
    }

    @TypeConverter
    fun fromStringToUser(data: String): User {
        val dataType = object : TypeToken<User>() {}.type
        return Gson().fromJson<User>(data, dataType)
    }

    @TypeConverter
    fun fromStringToKml(data: String): Kml {
        val dataType = object : TypeToken<Kml>() {}.type
        return Gson().fromJson<Kml>(data, dataType)
    }

    @TypeConverter
    fun fromStringToPoint(data: String): Point {
        val dataType = object : TypeToken<Point>() {}.type
        return Gson().fromJson<Point>(data, dataType)
    }

    @TypeConverter
    fun fromStringToDate(data: String): Date? {
        val dataType = object : TypeToken<Date>() {}.type
        return Gson().fromJson<Date>(data, dataType)
    }

    @TypeConverter
    fun fromDateToString(date: Date): String {
        val gson = Gson()
        return gson.toJson(date)
    }

    fun fromObjectToJsonString(data: Any): String {
        val gson = Gson()
        return gson.toJson(data)
    }

    fun fromObjectToArrayList(data: Any): ArrayList<ValuePair> {
        val dataString = fromObjectToJsonString(data)
        return fromStringToArrayList(dataString)
    }

    private fun fromStringToArrayList(dataString: String): ArrayList<ValuePair> {
        val result : ArrayList<ValuePair> = ArrayList()
        val json = JSONObject(dataString)
        val keys = json.keys()
        while (keys.hasNext()) {
            val key = keys.next()
            try {
                val value = json.getString(key)
                result.add(ValuePair(key, value))
            } catch (e: JSONException) {
                e.printStackTrace()
            }

        }
        return result
    }
}