package org.horizen.ongoingprojectmonitoring.Helper

import java.util.ArrayList
import java.util.concurrent.TimeUnit

import okhttp3.FormBody
import okhttp3.OkHttpClient
import okhttp3.Request

/**
 * Created by HORIZEN on 5/26/2017.
 */

object Server {

    const val serverUrl = "http://maps.wbphed.gov.in/ongoing_project_monitoring/mobile_service/"

    @Throws(Exception::class)
    fun callApi(api: String, valuePairs: ArrayList<ValuePair>): String {
        val client = OkHttpClient.Builder().connectTimeout(60, TimeUnit.SECONDS).readTimeout(60, TimeUnit.SECONDS).build()
        val builder = FormBody.Builder()
        for (vp in valuePairs) {
            builder.add(vp.name, vp.value)
        }
        val body = builder.build()
        val request = Request.Builder().url(api).post(body).build()
        val response = client.newCall(request).execute()
        return response.body()!!.string()
    }
}
