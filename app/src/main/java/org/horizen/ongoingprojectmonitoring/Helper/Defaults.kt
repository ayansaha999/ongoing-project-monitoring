package org.horizen.ongoingprojectmonitoring.Helper

object Defaults {
    const val LOGGED_IN = "loggedIn"

    const val PRIMARY_RISING_MAIN = "PRIMARY_RISING_MAIN"
    const val SECONDARY_RISING_MAIN = "SECONDARY_RISING_MAIN"
    const val PANCHAYAT = "PANCHAYAT"
    const val BLOCK = "BLOCK"
    const val ZONE = "ZONE"
    const val MOUZA = "MOUZA"
    const val SETTLEMENT = "SETTLEMENT"
    const val PROJECT = "PROJECT"
    const val ROAD = "ROAD"
    const val WATER_BODY = "WATER_BODY"
    const val PROPOSED_OHR = "PROPOSED_OHR"
    const val EXISTING_OHR = "EXISTING_OHR"

    const val GLR = "GLR"
    const val EXISTING_WTP = "EXISTING_WTP"
    const val PROPOSED_WTP = "PROPOSED_WTP"
    const val CWR = "CWR"
    const val BS = "BS"
    const val RBT = "RBT"
    const val PROPOSED_IP = "PROPOSED_IP"
    const val EXISTING_IP = "EXISTING_IP"

    const val PUMP_HOUSE = "PUMP_HOUSE"

}