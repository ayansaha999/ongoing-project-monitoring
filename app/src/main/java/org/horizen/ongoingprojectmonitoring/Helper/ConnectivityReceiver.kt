package org.horizen.ongoingprojectmonitoring.Helper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo

import org.horizen.ongoingprojectmonitoring.Root.RootApplication

/**
 * Created by Ayan Saha from HORIZEN on 6/16/2017.
 */

open class ConnectivityReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, arg1: Intent) {
        val cm = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = activeNetwork != null && activeNetwork.isConnected

        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }
    }


    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }

    companion object {

        var connectivityReceiverListener: ConnectivityReceiverListener? = null

        val isConnected: Boolean
            get() {
                val cm = RootApplication.instance!!.applicationContext
                        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
                val activeNetwork = cm!!.activeNetworkInfo
                return activeNetwork != null && activeNetwork.isConnected
            }
    }
}