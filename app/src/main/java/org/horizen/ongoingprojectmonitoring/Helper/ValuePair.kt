package org.horizen.ongoingprojectmonitoring.Helper

class ValuePair(_name: String, _value: String) {
    var name = ""
    var value = ""

    init {
        name = _name
        value = _value
    }
}