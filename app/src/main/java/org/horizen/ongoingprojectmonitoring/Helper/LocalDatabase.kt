package org.horizen.ongoingprojectmonitoring.Helper

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import org.horizen.ongoingprojectmonitoring.Daos.*
import org.horizen.ongoingprojectmonitoring.Daos.DiameterDao
import org.horizen.ongoingprojectmonitoring.Models.*

@Database(entities = [(User::class), (Kml::class), (Point::class), (Mouza::class), (Material::class),
    (Class::class), (LocationPoint::class), (Type::class), (Diameter::class), (ConstructionSite::class),
    (Agency::class), (ProgressHead::class), (ConstructionSiteImage::class), (ConstructionSiteVideo::class)], version = 3)
@TypeConverters(DataTypeConverter::class)
abstract class LocalDatabase: RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun kmlDao(): KmlDao

    abstract fun pointDao(): PointDao

    abstract fun mouzaDao(): MouzaDao

    abstract fun materialDao(): MaterialDao

    abstract fun classDao(): ClassDao

    abstract fun locationPointDao(): LocationPointDao

    abstract fun typeDao(): TypeDao

    abstract fun diameterDao(): DiameterDao

    abstract fun constructionSite(): ConstructionSiteDao

    abstract fun agencyDao(): AgencyDao

    abstract fun progressHeadDao(): ProgressHeadDao

    abstract fun constructionSiteImageDao(): ConstructionSiteImageDao

    abstract fun constructionSiteVideoDao(): ConstructionSiteVideoDao

}