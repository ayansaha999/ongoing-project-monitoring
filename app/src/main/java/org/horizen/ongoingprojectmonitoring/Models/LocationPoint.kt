package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class LocationPoint(

        @NonNull
        @PrimaryKey
        var id: String,

        var session_key: String,
        var project_id: String,
        var agency: String,
        var construction_site: String,
        var gps_lat: String,
        var gps_lon: String,
        var district_code: String,
        var blockcode: String,
        var gpcode: String,
        var mouzacode: String,
        var jl_no: String,
        var zone_id: String,
        var line_type: String,
        var locaion_type: String,
        var specials: String,
        var material: String,
        var class_: String,
        var angel: String,
        var diameter: String,
        var type: String,
        var size: String,
        var device_date: String,
        var device_time: String,
        var userid: String,
        var imagePath: String,
        var remarks: String,
        var status: Boolean

): Serializable {
    @Ignore
    constructor(): this("", "","", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "", "", "", "", "", "", "", "", "", false)

}