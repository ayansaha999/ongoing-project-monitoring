package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class Kml(

        @PrimaryKey
        var project_id: String,

        var center_lat: String,
        var center_lon: String,
        var project_name: String,
        var settlement: String,
        var project_boundary: String,
        var gp_boundary: String,
        var mouza_boundary: String,
        var communication: String,
        var primery_rising_main: String,
        var secondary_rising_main: String,
        var water_body: String,
        var block_boundary: String,
        var zone_boundary: String

): Serializable {
    @Ignore
    constructor(): this("", "", "", "", "", "",
            "", "", "", "", "", "",
            "", "")
}