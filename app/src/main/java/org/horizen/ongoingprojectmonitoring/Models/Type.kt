package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class Type(
        @NonNull
        @PrimaryKey
        var serial: String,

        var special_id: String,
        var type_id: String,
        var type_name: String
): Serializable {
    constructor(): this("", "", "", "")
}