package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class ConstructionSiteImage(
        @NonNull
        @PrimaryKey
        var id: String,
        var point_id: String,
        var project_id: String,
        var gps_lat: String,
        var gps_lon: String,
        var user_id: String,
        var image_captionn: String,
        var progress_type_id: String,
        var file_path: String,
        var status: Boolean
): Serializable {

    @Ignore
    constructor(): this("", "", "", "", "", "", "", "", "",false)
}