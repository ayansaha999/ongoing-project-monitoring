package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class Agency(

        @PrimaryKey
        var serial: String,

        var agency_id: String,

        var agency_name: String,

        var line_type: String,

        var blockid: String
): Serializable {
    constructor(): this("", "", "", "", "")
}