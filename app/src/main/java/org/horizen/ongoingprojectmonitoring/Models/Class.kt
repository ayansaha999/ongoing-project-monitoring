package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class Class (

        var material: String,

        var class_: String,

        @NonNull
        @PrimaryKey
        var serial: String

): Serializable {

    @Ignore
    constructor(): this("", "", "")
}