package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class User(

        @PrimaryKey
        var user_id: String,

        var uname: String,

        var password: String,

        var dist_id: String,

        var status: Boolean,

        var project_id: String,

        var version: String
): Serializable {
    @Ignore
    constructor(): this("", "", "", "", false, "", "")
}