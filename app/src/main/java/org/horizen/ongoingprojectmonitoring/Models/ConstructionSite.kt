package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class ConstructionSite(

        @NonNull
        @PrimaryKey
        var serial: String,

        var construction_site: String

): Serializable {
    constructor(): this("", "")
}