package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class Material(

        @NonNull
        @PrimaryKey
        var serial: String,

        var material: String,

        var special_id: String

): Serializable {

    @Ignore
    constructor(): this("", "", "")
}