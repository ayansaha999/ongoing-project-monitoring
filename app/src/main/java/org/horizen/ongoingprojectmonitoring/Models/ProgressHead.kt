package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class ProgressHead(
        var progress_id: String,
        var progress_type: String,
        var specials_id: String,

        @PrimaryKey
        var serial: String
): Serializable {

    @Ignore
    constructor(): this("", "", "", "")

}