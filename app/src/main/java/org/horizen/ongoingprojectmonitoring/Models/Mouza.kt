package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class Mouza(

        @NonNull
        @PrimaryKey
        var serial: String,

        var project_id: String,

        var district_code: String,
        var blockcode: String,
        var blockname: String,
        var panchayat_code: String,
        var vill_code: String,
        var vill_name: String,
        var jl_no: String,
        var zone_code: String,
        var zone_name: String

): Serializable {

    @Ignore
    constructor(): this("", "","", "","", "",
            "", "","", "", "")

}

