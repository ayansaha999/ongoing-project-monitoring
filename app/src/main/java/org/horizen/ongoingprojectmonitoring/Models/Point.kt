package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Ignore
import android.arch.persistence.room.PrimaryKey
import java.io.Serializable

@Entity
data class Point(

        @PrimaryKey
        var id: String,

        var project_id: String,

        var block_name: String,
        var panchayat_name: String,
        var mouza_name: String,
        var habitation_name: String,
        var location: String,
        var zone_no: String,
        var lat: String,
        var lon: String,
        var location_name: String,
        var location_type: String,
        var capacity: String,
        var height: String,
        var image_path: String,
        var progress_type_id: String,
        var remarks: String,
        var status: Int,

        var newPoint: Boolean

): Serializable {
    @Ignore
    constructor(): this("", "","", "", "", "", "",
            "", "", "", "", "", "", "", "", "",
            "",0, false)
}