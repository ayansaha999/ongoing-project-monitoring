package org.horizen.ongoingprojectmonitoring.Models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import java.io.Serializable

@Entity
data class Diameter(

        @NonNull
        @PrimaryKey
        var serial: String,

        var special_id: String,
        var material: String,
        var diameter: String
): Serializable {
    constructor(): this("", "", "", "")
}