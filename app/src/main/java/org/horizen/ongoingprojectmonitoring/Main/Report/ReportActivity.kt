package org.horizen.ongoingprojectmonitoring.Main.Report

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.horizen.ongoingprojectmonitoring.R

import kotlinx.android.synthetic.main.activity_report.*
import kotlinx.android.synthetic.main.content_report.*
import com.anychart.anychart.AnyChart.cartesian
import android.R.attr.data
import android.arch.lifecycle.Observer
import android.content.Intent
import com.anychart.anychart.*
import com.anychart.anychart.AnyChart.column
import org.horizen.ongoingprojectmonitoring.Root.RootActivity
import com.anychart.anychart.DataEntry
import org.horizen.ongoingprojectmonitoring.Helper.ValuePair
import org.horizen.ongoingprojectmonitoring.Main.Home.HomeActivity
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteImage
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteVideo


class ReportActivity : RootActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)

        back.setOnClickListener {
            finish()
        }

        rootVm.locationPointList?.observe(this, Observer { locationPoints ->
            rootVm.setUploadStatusSummary()
            if (locationPoints != null && locationPoints.isNotEmpty()) {
                val datesArray = ArrayList<String>()
                for (locationPoint in locationPoints) {
                    if (!datesArray.contains(locationPoint.device_date)) {
                        datesArray.add(locationPoint.device_date)
                    }
                }
                datesArray.sort()

                val listItems = ArrayList<ValuePair>()
                for (date in datesArray) {
                    var pointsCount = 0
                    for (locationPoint in locationPoints) {
                        if (locationPoint.device_date == date) {
                            pointsCount += 1
                        }
                    }

                    listItems.add(ValuePair(date, "$pointsCount location points."))
                }
            }
        })

        rootVm.imagesList?.observe(this, Observer { images ->
            rootVm.setUploadStatusSummary()
        })

        rootVm.videoList?.observe(this, Observer { videos ->
            rootVm.setUploadStatusSummary()
        })

        rootVm.uploadStatusSummary.observe(this, Observer { value ->
            summary.text = value
        })


    }
}
