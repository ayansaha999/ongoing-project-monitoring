package org.horizen.ongoingprojectmonitoring.Main.Auth

import android.Manifest
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.view.View
import android.view.WindowManager
import kotlinx.android.synthetic.main.content_auth.*

import org.horizen.ongoingprojectmonitoring.Helper.Defaults
import org.horizen.ongoingprojectmonitoring.Root.RootActivity
import org.horizen.ongoingprojectmonitoring.Main.Home.HomeActivity
import org.horizen.ongoingprojectmonitoring.R
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.toast

class AuthActivity : RootActivity() {

    @SuppressLint("SetTextI18n", "PrivateResource")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        setContentView(R.layout.activity_auth)

        if(prefs.getBoolean(Defaults.LOGGED_IN)) {
            startActivity(Intent(this@AuthActivity, HomeActivity::class.java))
            finish()
            return
        }

        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))

        prefs.putBoolean(Defaults.BLOCK, false)
        prefs.putBoolean(Defaults.PROJECT, true)
        prefs.putBoolean(Defaults.MOUZA, false)
        prefs.putBoolean(Defaults.PANCHAYAT, true)
        prefs.putBoolean(Defaults.ZONE, false)
        prefs.putBoolean(Defaults.ROAD, true)
        prefs.putBoolean(Defaults.SETTLEMENT, false)
        prefs.putBoolean(Defaults.WATER_BODY, false)
        prefs.putBoolean(Defaults.PRIMARY_RISING_MAIN, true)
        prefs.putBoolean(Defaults.SECONDARY_RISING_MAIN, true)
        prefs.putBoolean(Defaults.EXISTING_OHR, true)
        prefs.putBoolean(Defaults.PROPOSED_OHR, true)
        prefs.putBoolean(Defaults.CWR, true)
        prefs.putBoolean(Defaults.EXISTING_WTP, true)
        prefs.putBoolean(Defaults.EXISTING_IP, true)
        prefs.putBoolean(Defaults.PROPOSED_WTP, true)
        prefs.putBoolean(Defaults.PROPOSED_IP, true)
        prefs.putBoolean(Defaults.GLR, true)
        prefs.putBoolean(Defaults.BS, true)
        prefs.putBoolean(Defaults.RBT, true)
        prefs.putBoolean(Defaults.PUMP_HOUSE, true)

        actionButton.setOnClickListener { _ ->
            when (rootVm.authstatus.value) {
                0 -> {
                    val password = password.text.toString()
                    val username = username.text.toString()

                    if (username.isEmpty()) {
                        toast("Enter username!")
                        return@setOnClickListener
                    }

                    if (password.isEmpty()) {
                        toast("Enter username!")
                        return@setOnClickListener
                    }

                    rootVm.authstatus.value = 1
                    login(username, password)
                }
                2 -> {
                    checkPermissions()
                }
                3 -> {
                    rootVm.authstatus.value = 4
                    downloadKmlData(rootVm.activeUser.value!!)
                }
            }
        }

        rootVm.authstatus.observe(this, Observer { status ->
            when (status) {
                0 -> {
                    actionText.text = "Login"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.VISIBLE
                    actionDetails.visibility = View.GONE
                }
                1 -> {
                    actionText.visibility = View.GONE
                    actionProgress.visibility = View.VISIBLE
                    loginLay.visibility = View.VISIBLE
                    actionDetails.visibility = View.GONE
                }
                2 -> {
                    if(checkPermissions()) {
                        rootVm.authstatus.value = 3
                    }
                    actionText.text = "Agree"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                    rootVm.actionMessage.value = "This app needs permission to store mouza_data and media files in your device, get location updates, record audio and access your device camera."
                }
                3 -> {
                    actionText.text = "Download"
                    actionText.visibility = View.VISIBLE
                    actionProgress.visibility = View.GONE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                    rootVm.actionMessage.value = "Your assigned location details, form data, kml files and point mouza_data for map will be downloaded. Click the download button to download all data."
                }
                4 -> {
                    actionText.visibility = View.GONE
                    actionProgress.visibility = View.VISIBLE
                    loginLay.visibility = View.GONE
                    actionDetails.visibility = View.VISIBLE
                }
            }
        })

        rootVm.actionMessage.observe(this, Observer { message ->
            actionDetails.text = message
        })

        togglePassVisibility.setOnClickListener {
            rootVm.passwordVisible.value = !rootVm.passwordVisible.value!!
        }

        rootVm.passwordVisible.observe(this, Observer { value ->
            if (value != null && value) {
                password.inputType = InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
                password.setSelection(password.length())
                togglePassVisibility.imageResource = R.drawable.design_ic_visibility_off
            } else {
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                password.setSelection(password.length())
                togglePassVisibility.imageResource = R.drawable.design_ic_visibility
            }
        })
    }

    private fun checkPermissions(): Boolean {
        return if (ContextCompat.checkSelfPermission(this@AuthActivity, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            requestPermissions()
            false
        }
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(this@AuthActivity,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.RECORD_AUDIO),
                REQUEST_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) { REQUEST_PERMISSION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    rootVm.authstatus.value = 3
                } else {
                   toast("Permission Denied!")
                }
                return
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}
