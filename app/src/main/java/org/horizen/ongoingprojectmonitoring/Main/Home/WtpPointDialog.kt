package org.horizen.ongoingprojectmonitoring.Main.Home

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.kbeanie.multipicker.api.CameraImagePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenImage
import kotlinx.android.synthetic.main.dialog_wtp_point_details.*
import org.horizen.ongoingprojectmonitoring.Helper.Util
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteImage
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteVideo
import org.horizen.ongoingprojectmonitoring.Models.Point
import org.horizen.ongoingprojectmonitoring.Models.ProgressHead
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootDialogFragment
import org.horizen.ongoingprojectmonitoring.Root.RootFragment
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onCheckedChange
import java.io.File
import java.util.*

class WtpPointDialog: RootDialogFragment(), ImagePickerCallback {
    private var pickerPath: String? = null

    private lateinit var point: Point

    private var statusheadArray = ArrayList<ProgressHead>()
    private var currProgressTypeId = ""

    companion object {

        @JvmStatic
        fun newInstance(point: Point) = WtpPointDialog().apply {
            val b = Bundle()
            b.putSerializable("point", point)
            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_wtp_point_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        point = arguments?.getSerializable("point") as Point

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        loadStatusHead()
    }

    fun loadStatusHead() {
        doAsync {
            val statusHeadList = localDb.progressHeadDao().getAllProgressHeads()
            for (statusHead in statusHeadList) {
                if (statusHead.specials_id == "4") {
                    statusheadArray.add(statusHead)

                }
            }

            activity!!.runOnUiThread {
                val statusAd = StatusHeadAdapter(statusheadArray)
                listView.adapter = statusAd
            }
        }
    }

    private var cameraPicker: CameraImagePicker? = null

    private fun capture() {
        cameraPicker = CameraImagePicker(this)
        cameraPicker!!.setDebugglable(true)
        cameraPicker!!.setImagePickerCallback(this)
        pickerPath = cameraPicker!!.pickImage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = CameraImagePicker(this)
                    cameraPicker!!.setImagePickerCallback(this)
                    cameraPicker!!.reinitialize(pickerPath)
                }
                cameraPicker!!.submit(data)
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                val videoUri = Uri.parse(data?.dataString)
                val path = Util.getRealPathFromURI(activity!!.applicationContext, videoUri)
                Log.e("Video Path", path)
                val csImage = ConstructionSiteVideo()
                csImage.id = UUID.randomUUID().toString()
                csImage.point_id = point.id
                csImage.user_id = prefs.getString("user_id")
                csImage.gps_lat = point.lat
                csImage.gps_lon = point.lon
                csImage.file_path = path
                csImage.progress_type_id = currProgressTypeId
                (activity as HomeActivity).showRemarksDialog(null, csImage, 1)
            }
        }
    }

    override fun onImagesChosen(list: MutableList<ChosenImage>?) {
        Log.e("Image Picker", "Choosen")
        for (i in list!!.indices) {
            val m = list[i]
            val p = m.originalPath
            val currPath = getOutputMediaFileUri()
            Util.resizeImage(p, currPath.absolutePath)
            val currImagePath = currPath.absolutePath
            Log.e("Image Path", currImagePath)
            val csImage = ConstructionSiteImage()
            csImage.id = UUID.randomUUID().toString()
            csImage.point_id = point.id
            csImage.user_id = prefs.getString("user_id")
            csImage.gps_lat = point.lat
            csImage.gps_lon = point.lon
            csImage.file_path = currImagePath
            csImage.progress_type_id = currProgressTypeId
            (activity as HomeActivity).showRemarksDialog(csImage, null, 0)
        }
    }

    private fun getOutputMediaFileUri(): File {
        var uploadDir = File(Environment.getExternalStorageDirectory().absolutePath + "/CPP")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        uploadDir = File(uploadDir, "images")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        return File(uploadDir, System.currentTimeMillis().toString() + ".jpg")
    }

    override fun onError(p0: String?) {

    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as HomeActivity).cancelPointCreation()
    }

    inner class StatusHeadAdapter(var statusHeads: ArrayList<ProgressHead>): BaseAdapter() {
        private val mInflater: LayoutInflater = LayoutInflater.from(activity)

        @SuppressLint("ViewHolder")
        override fun getView(position: Int, p1: View?, parent: ViewGroup?): View {
            val view = mInflater.inflate(R.layout.single_status_field, parent, false)

            val checkBox = view.findViewById<TextView>(R.id.title)
            val captureImage = view.findViewById<Button>(R.id.capture_image)
            val captureVideo = view.findViewById<Button>(R.id.capture_video)
            val close = view.findViewById<ImageView>(R.id.close)

            val statusHead = statusHeads[position]
            checkBox.text = statusHead.progress_type
            captureImage.text = "Capture Image of ${statusHead.progress_type}"
            captureVideo.text = "Capture Video of ${statusHead.progress_type}"

            captureImage.setOnClickListener {
                currProgressTypeId = statusHead.progress_id
                capture()
            }

            captureVideo.setOnClickListener {
                currProgressTypeId = statusHead.progress_id
                captureVideo()
            }

            close.setOnClickListener {
                if (rootVm.activeCheckox.value == statusHead.progress_id) {
                    rootVm.activeCheckox.value = ""
                    captureImage.visibility = View.GONE
                    captureVideo.visibility = View.GONE
                    close.setImageResource(R.drawable.ic_down)
                } else {
                    rootVm.activeCheckox.value = statusHead.progress_id
                    captureImage.visibility = View.VISIBLE
                    captureVideo.visibility = View.VISIBLE
                    close.setImageResource(R.drawable.ic_up)
                }
            }

            if (rootVm.activeCheckox.value == statusHead.progress_id) {
                captureImage.visibility = View.VISIBLE
                captureVideo.visibility = View.VISIBLE
                close.setImageResource(R.drawable.ic_up)
            } else {
                captureImage.visibility = View.GONE
                captureVideo.visibility = View.GONE
                close.setImageResource(R.drawable.ic_down)
            }

            rootVm.activeCheckox.observe(this@WtpPointDialog, Observer { value ->
                if (value == statusHead.progress_id) {
                    captureImage.visibility = View.VISIBLE
                    captureVideo.visibility = View.VISIBLE
                    close.setImageResource(R.drawable.ic_up)
                } else {
                    captureImage.visibility = View.GONE
                    captureVideo.visibility = View.GONE
                    close.setImageResource(R.drawable.ic_down)
                }
            })
            return view
        }

        override fun getItem(p0: Int): Any {
            return statusHeads[p0]
        }

        override fun getItemId(p0: Int): Long {
            return p0.toLong()
        }

        override fun getCount(): Int {
            return statusHeads.size
        }
    }

    fun captureVideo() {
        val takeVideoIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        takeVideoIntent.putExtra(MediaStore.EXTRA_OUTPUT, Environment.getExternalStorageDirectory().path + "${UUID.randomUUID()}.mp4")
        startActivityForResult(takeVideoIntent, REQUEST_VIDEO_CAPTURE)
    }
}