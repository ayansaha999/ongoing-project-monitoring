package org.horizen.ongoingprojectmonitoring.Main.Home

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.kbeanie.multipicker.api.CameraImagePicker
import com.kbeanie.multipicker.api.Picker
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback
import com.kbeanie.multipicker.api.entity.ChosenImage
import kotlinx.android.synthetic.main.dialog_dist_main_details.*
import org.horizen.ongoingprojectmonitoring.Helper.Util
import org.horizen.ongoingprojectmonitoring.Models.*
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootDialogFragment
import org.jetbrains.anko.doAsync
import java.io.File
import java.util.*
import kotlin.collections.ArrayList

class DistMainDialog : RootDialogFragment(), ImagePickerCallback {

    private val mouzaDataList = ArrayList<Mouza>()
    private val materialDataList = ArrayList<Material>()
    private val classDataList = ArrayList<Class>()
    private val typesDataList = ArrayList<Type>()
    private val diameterDataList = ArrayList<Diameter>()
    private var agencyDataList = ArrayList<Agency>()

    private val zoneIdArray = ArrayList<String>()
    private val zoneNameArray = ArrayList<String>()

    private val villageIdArray = ArrayList<String>()
    private val villageNameArray = ArrayList<String>()

    private val agencyIdArray = ArrayList<String>()
    private val agencyNameArray = ArrayList<String>()

    private val locationArray = ArrayList<String>()

    private val specialIdArray = ArrayList<Int>()
    private val specialArray = ArrayList<String>()

    private val materialArray = ArrayList<String>()
    private val diameterArray = ArrayList<String>()

    private val angleArray = ArrayList<String>()

    private val thrustArray = ArrayList<String>()

    private val typeIdArray = ArrayList<String>()
    private val typeNameArray = ArrayList<String>()

    private val classArray = ArrayList<String>()

    private val currentPointData = LocationPoint()
    private val currentImageData = ConstructionSiteImage()

    private var pickerPath: String? = null

    companion object {

        @JvmStatic
        fun newInstance() = DistMainDialog().apply { }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_dist_main_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        thrustArray.add("No")
        thrustArray.add("Yes")

        cancel.setOnClickListener {
            (activity as HomeActivity).cancelPointCreation()
            dialog.dismiss()
        }

        save.setOnClickListener {
            val redS1 = reducer_size_1.text.toString()
            val redS2  = reducer_size_2.text.toString()

            val teeS1 = tee_size_1.text.toString()
            val teeS2  = tee_size_2.text.toString()
            val teeS3  = tee_size_3.text.toString()

            val remark = remarks.text.toString()

            val size = if (redS1.isNotEmpty() && redS2.isNotEmpty()) {
                "$redS1*$redS2"
            } else if (teeS1.isNotEmpty() && teeS2.isNotEmpty() && teeS3.isNotEmpty()) {
                "$teeS1*$teeS2*$teeS3"
            } else {
                ""
            }

            currentPointData.size = size
            currentPointData.remarks = remark
            (activity as HomeActivity).saveLocationPoint(currentPointData, currentImageData)
            dialog.dismiss()
        }

        capture_image.setOnClickListener {
            capture()
        }

        currentPointData.id = UUID.randomUUID().toString()
        currentPointData.line_type = 3.toString()
        currentPointData.device_date = getDeviceDate()
        currentPointData.device_time = getDeviceTime()

        locationArray.add("Start Point")
        locationArray.add("Intermediate Point")
        locationArray.add("End Point")
        locationArray.add("Critical Area Start")
        locationArray.add("Critical Area End")
        locationArray.add("Crossing")

        specialIdArray.add(0)
        specialIdArray.add(6)
        specialIdArray.add(7)
        specialIdArray.add(8)
        specialIdArray.add(9)
        specialIdArray.add(13)
        specialIdArray.add(14)
        specialIdArray.add(11)
        specialIdArray.add(12)

        specialArray.add("Select")
        specialArray.add("Bend")
        specialArray.add("Tee")
        specialArray.add("Joint")
        specialArray.add("Reducer")
        specialArray.add("Sluice Valve")
        specialArray.add("Butterfly Valve")
        specialArray.add("Air Valve")
        specialArray.add("Caller Joint")

        angleArray.add(getString(R.string.eleven_one_forth))
        angleArray.add(getString(R.string.twenty_two_half))
        angleArray.add("45")
        angleArray.add("90")

        select_special.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
                currentPointData.specials = specialIdArray[position].toString()
                when (currentPointData.specials.toInt()) {
                    0 -> {
                        bendLay.visibility = View.GONE
                        teeLay.visibility = View.GONE
                        jointLay.visibility = View.GONE
                        reducerLay.visibility = View.GONE
                        valveLay.visibility = View.GONE
                        airValveLay.visibility = View.GONE
                        callerLay.visibility = View.GONE
                    }
                    6 -> {
                        loadBend()
                    }
                    7 -> {
                        loadTee()
                    }
                    8 -> {
                        loadJoint()
                    }
                    9 -> {
                        loadReducer()
                    }
                    13 -> {
                        loadValve()
                    }
                    14 -> {
                        loadValve()
                    }
                    11 -> {
                        loadAirValve()
                    }
                    12 -> {
                        loadCaller()
                    }
                    15 -> {
                        loadCrossing()
                    }
                }
            }
        }

        loadZones()

        select_zone.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.zone_id = zoneIdArray[p2]
                loadVillage()
            }
        }

        select_village.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.mouzacode = villageIdArray[p2]
                loadLocation()
            }
        }

        select_location.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.locaion_type = (1 + p2).toString()
                if (p2 == 5) {
                    specials_lay.visibility = View.GONE
                    loadCrossing()
                } else {
                    specials_lay.visibility = View.VISIBLE
                    crossingLay.visibility = View.GONE
                }
            }
        }

        select_agency.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.agency = agencyIdArray[p2]
            }
        }

        val locationTypeAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, locationArray)
        locationTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_location.adapter = locationTypeAd
        select_location.setSelection(0)

        val specialTypeAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, specialArray)
        specialTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_special.adapter = specialTypeAd
        select_special.setSelection(0)

        val thrustAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, thrustArray)
        thrustAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_bend_thrust.adapter = thrustAd
        select_bend_thrust.setSelection(0)
    }


    private fun loadLocation() {
        for (mouza in mouzaDataList) {
            if (mouza.zone_code == currentPointData.zone_id && mouza.vill_code == currentPointData.mouzacode) {
                currentPointData.district_code = mouza.district_code
                currentPointData.blockcode = mouza.blockcode
                currentPointData.gpcode = mouza.panchayat_code
                currentPointData.jl_no = mouza.jl_no

                for (agency in agencyDataList) {
                    if (agency.blockid.contains(mouza.blockcode) && agency.line_type == "3" && !agencyIdArray.contains(agency.agency_id)) {
                        agencyIdArray.add(agency.agency_id)
                        agencyNameArray.add(agency.agency_name)
                    }
                }

                val agencyAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, agencyNameArray)
                agencyAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_agency.adapter = agencyAd
                select_agency.setSelection(0)
            }
        }
    }

    private fun loadVillage() {
        villageIdArray.clear()
        villageNameArray.clear()
        for (mouza in mouzaDataList) {
            if (mouza.zone_code == currentPointData.zone_id) {
                if (!villageIdArray.contains(mouza.vill_code)) {
                    villageIdArray.add(mouza.vill_code)
                    villageNameArray.add(mouza.vill_name)
                }
            }
        }

        val villageTypeAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, villageNameArray)
        villageTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_village.adapter = villageTypeAd
        select_village.setSelection(0)
    }

    private fun loadZones() {
        zoneIdArray.clear()
        zoneNameArray.clear()
        doAsync {
            val mouzaData = localDb.mouzaDao().getAllMouzas()
            val materialData = localDb.materialDao().getAllMaterials()
            val classData = localDb.classDao().getAllClasss()
            val typeData = localDb.typeDao().getAllTypes()
            val diameterData = localDb.diameterDao().getAllDiameters()
            val agencyData = localDb.agencyDao().getAllAgencys()

            mouzaDataList.addAll(mouzaData)
            classDataList.addAll(classData)
            materialDataList.addAll(materialData)
            typesDataList.addAll(typeData)
            diameterDataList.addAll(diameterData)
            agencyDataList.addAll(agencyData)

            for (mouza in mouzaDataList) {
                if (!zoneIdArray.contains(mouza.zone_code)) {
                    zoneIdArray.add(mouza.zone_code)
                    zoneNameArray.add(mouza.zone_name)
                }
            }

            activity!!.runOnUiThread {

                val zoneTypeAd = ArrayAdapter<String>(activity!!, R.layout.spinner_list_item, zoneNameArray)
                zoneTypeAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_zone.adapter = zoneTypeAd
                select_zone.setSelection(0)
            }
        }
    }


    private fun loadBend() {
        bendLay.visibility = View.VISIBLE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        materialArray.clear()
        for (material in materialDataList) {
            if (!materialArray.contains(material.material)) {
                if (material.special_id == currentPointData.specials) {
                    materialArray.add(material.material)
                }
            }
        }

        val materialAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, materialArray)
        materialAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_bend_material.adapter = materialAd
        select_bend_material.setSelection(0)

        select_bend_material.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.material = materialArray[p2]

                diameterArray.clear()
                for (diameter in diameterDataList) {
                    if (diameter.special_id == currentPointData.specials && diameter.material == currentPointData.material) {
                        if (!diameterArray.contains(diameter.diameter)) {
                            diameterArray.add(diameter.diameter)
                        }
                    }
                }

                val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
                diaAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_bend_diameter.adapter = diaAd
                select_bend_diameter.setSelection(0)

                select_bend_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.diameter = diameterArray[p2]
                    }
                }
            }
        }

        val angleAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, angleArray)
        angleAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_bend_angle.adapter = angleAd
        select_bend_angle.setSelection(0)

        select_bend_angle.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.angel = "${p2 + 1}"
            }
        }

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (!typeIdArray.contains(type.type_id)) {
                if (type.special_id == currentPointData.specials) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_bend_type.adapter = typeAd
        select_bend_type.setSelection(0)

        select_bend_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }

        }
    }

    private fun loadTee() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.VISIBLE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        materialArray.clear()
        for (material in materialDataList) {
            if (!materialArray.contains(material.material)) {
                if (material.special_id == currentPointData.specials) {
                    materialArray.add(material.material)
                }
            }
        }

        select_tee_material.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.material = materialArray[p2]

                diameterArray.clear()
                for (diameter in diameterDataList) {
                    if (diameter.special_id == currentPointData.specials && diameter.material == currentPointData.material) {
                        if (!diameterArray.contains(diameter.diameter)) {
                            diameterArray.add(diameter.diameter)
                        }
                    }
                }

                val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
                diaAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_tee_diameter.adapter = diaAd
                select_tee_diameter.setSelection(0)

                select_tee_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.diameter = diameterArray[p2]
                    }
                }
            }
        }

        val materialAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, materialArray)
        materialAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_tee_material.adapter = materialAd
        select_tee_material.setSelection(0)

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (!typeIdArray.contains(type.type_id)) {
                if (type.special_id == currentPointData.specials) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_tee_type.adapter = typeAd
        select_tee_type.setSelection(0)

        select_tee_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }

        }
    }

    private fun loadJoint() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.VISIBLE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        materialArray.clear()
        for (material in materialDataList) {
            if (!materialArray.contains(material.material)) {
                if (material.special_id == currentPointData.specials) {
                    materialArray.add(material.material)
                }
            }
        }

        select_joint_material.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.material = materialArray[p2]

                diameterArray.clear()
                for (diameter in diameterDataList) {
                    if (diameter.special_id == currentPointData.specials && diameter.material == currentPointData.material) {
                        if (!diameterArray.contains(diameter.diameter)) {
                            diameterArray.add(diameter.diameter)
                        }
                    }
                }

                val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
                diaAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_joint_diameter.adapter = diaAd
                select_joint_diameter.setSelection(0)

                select_joint_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.diameter = diameterArray[p2]
                    }
                }

                classArray.clear()
                for (material in classDataList) {
                    if (material.material == currentPointData.material) {
                        if (!classArray.contains(material.class_)) {
                            classArray.add(material.class_)
                        }
                    }
                }

                val classAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, classArray)
                classAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_joint_class.adapter = classAd
                select_joint_class.setSelection(0)

                select_joint_class.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.class_ = classArray[p2]
                    }
                }

            }
        }

        val materialAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, materialArray)
        materialAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_joint_material.adapter = materialAd
        select_joint_material.setSelection(0)

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (!typeIdArray.contains(type.type_id)) {
                if (type.special_id == currentPointData.specials) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_joint_type.adapter = typeAd
        select_joint_type.setSelection(0)

        select_joint_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }
        }
    }

    private fun loadReducer() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.VISIBLE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        materialArray.clear()
        for (material in materialDataList) {
            if (!materialArray.contains(material.material)) {
                if (material.special_id == currentPointData.specials) {
                    materialArray.add(material.material)
                }
            }
        }

        select_reducer_material.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.material = materialArray[p2]

                diameterArray.clear()
                for (diameter in diameterDataList) {
                    if (diameter.special_id == currentPointData.specials && diameter.material == currentPointData.material) {
                        if (!diameterArray.contains(diameter.diameter)) {
                            diameterArray.add(diameter.diameter)
                        }
                    }
                }

                val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
                diaAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_reducer_diameter.adapter = diaAd
                select_reducer_diameter.setSelection(0)

                select_reducer_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.diameter = diameterArray[p2]
                    }
                }
            }
        }

        val materialAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, materialArray)
        materialAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_reducer_material.adapter = materialAd
        select_reducer_material.setSelection(0)

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (!typeIdArray.contains(type.type_id)) {
                if (type.special_id == currentPointData.specials) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_reducer_type.adapter = typeAd
        select_reducer_type.setSelection(0)

        select_reducer_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }

        }
    }

    private fun loadValve() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.VISIBLE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        diameterArray.clear()
        for (diameter in diameterDataList) {
            if (diameter.special_id == currentPointData.specials) {
                if (!diameterArray.contains(diameter.diameter)) {
                    diameterArray.add(diameter.diameter)
                }
            }
        }

        val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
        diaAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_valve_diameter.adapter = diaAd
        select_valve_diameter.setSelection(0)

        select_valve_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.diameter = diameterArray[p2]
            }
        }

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (type.special_id == currentPointData.specials) {
                if (!typeIdArray.contains(type.type_id)) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_valve_type.adapter = typeAd
        select_valve_type.setSelection(0)

        select_valve_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }
        }
    }

    private fun loadAirValve() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.VISIBLE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.GONE

        diameterArray.clear()
        for (diameter in diameterDataList) {
            if (diameter.special_id == currentPointData.specials) {
                if (!diameterArray.contains(diameter.diameter)) {
                    diameterArray.add(diameter.diameter)
                }
            }
        }

        val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
        diaAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_air_valve_diameter.adapter = diaAd
        select_air_valve_diameter.setSelection(0)

        select_air_valve_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.diameter = diameterArray[p2]
            }
        }

        typeIdArray.clear()
        typeNameArray.clear()
        for (type in typesDataList) {
            if (type.special_id == currentPointData.specials) {
                if (!typeIdArray.contains(type.type_id)) {
                    typeIdArray.add(type.type_id)
                    typeNameArray.add(type.type_name)
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_air_valve_type.adapter = typeAd
        select_air_valve_type.setSelection(0)

        select_air_valve_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]
            }
        }
    }

    private val pipeJointTypes = ArrayList<String>()
    private val jackPushingTypes = ArrayList<String>()

    private fun loadCrossing() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.GONE
        crossingLay.visibility = View.VISIBLE

        pipeJointTypes.clear()
        jackPushingTypes.clear()

        pipeJointTypes.add("Cut")
        pipeJointTypes.add("Cover")

        jackPushingTypes.add("Casing Pipe")
        jackPushingTypes.add("Water Main")

        typeIdArray.clear()
        typeNameArray.clear()

        currentPointData.specials = "15"

        typeIdArray.addAll(arrayOf("1", "2", "3", "4", "5", "6"))
        typeNameArray.addAll(arrayOf("Due to distress", "Rail", "Road", "Murshy Land", "Culvert", "Canal/Khal/Nalas"))

        select_crossing_type.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.type = typeIdArray[p2]

                val techArrangementList = ArrayList<String>()
                when ((p2 + 1).toString()) {
                    "2" -> {
                        techArrangementList.clear()
                        techArrangementList.add("Pipe Pushing")
                        techArrangementList.add("Jack Pushing")
                        piers_lay.visibility = View.GONE
                        bridge_lay.visibility = View.GONE
                        jack_pushing_lay.visibility = View.GONE
                    }
                    "3" -> {
                        techArrangementList.clear()
                        techArrangementList.add("Pipe Pushing")
                        techArrangementList.add("Jack Pushing")
                        piers_lay.visibility = View.GONE
                        bridge_lay.visibility = View.GONE
                        jack_pushing_lay.visibility = View.GONE
                    }
                    "4" -> {
                        techArrangementList.clear()
                        techArrangementList.add("Piers")
                        piers_lay.visibility = View.VISIBLE
                        bridge_lay.visibility = View.GONE
                        jack_pushing_lay.visibility = View.GONE
                    }
                    "6" -> {
                        techArrangementList.clear()
                        techArrangementList.add("Bridge")
                        piers_lay.visibility = View.GONE
                        bridge_lay.visibility = View.VISIBLE
                        jack_pushing_lay.visibility = View.GONE
                    }
                    else -> {
                        piers_lay.visibility = View.GONE
                        bridge_lay.visibility = View.GONE
                        jack_pushing_lay.visibility = View.GONE
                    }
                }

                val techAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, techArrangementList)
                techAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_tech_arrangement.adapter = techAd
                select_tech_arrangement.setSelection(0)
            }
        }

        select_tech_arrangement.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                if (p2 > 0) {
                    jack_pushing_lay.visibility = View.VISIBLE
                } else {
                    jack_pushing_lay.visibility = View.GONE
                }
            }
        }

        val typeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        typeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_crossing_type.adapter = typeAd
        select_crossing_type.setSelection(0)

        val pipeJointAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, pipeJointTypes)
        pipeJointAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_pipe_joint.adapter = pipeJointAd
        select_pipe_joint.setSelection(0)

        val jackAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, jackPushingTypes)
        jackAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_jack_pushing.adapter = jackAd
        select_jack_pushing.setSelection(0)

        val piersAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, thrustArray)
        piersAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_piers.adapter = piersAd
        select_piers.setSelection(0)

        val bridgeAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, thrustArray)
        bridgeAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_bridge.adapter = bridgeAd
        select_bridge.setSelection(0)
    }

    private fun loadCaller() {
        bendLay.visibility = View.GONE
        teeLay.visibility = View.GONE
        jointLay.visibility = View.GONE
        reducerLay.visibility = View.GONE
        valveLay.visibility = View.GONE
        airValveLay.visibility = View.GONE
        callerLay.visibility = View.VISIBLE
        crossingLay.visibility = View.GONE

        typeIdArray.clear()
        typeNameArray.clear()
        for (material in typesDataList) {
            if (material.special_id == currentPointData.specials) {
                if (!typeIdArray.contains(material.type_id)) {
                    typeIdArray.add(material.type_id)
                    typeNameArray.add(material.type_name)
                }
            }
        }

        select_caller_material.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                currentPointData.material = typeNameArray[p2]

                diameterArray.clear()
                for (diameter in diameterDataList) {
                    if (diameter.special_id == currentPointData.specials && diameter.material == currentPointData.material) {
                        if (!diameterArray.contains(diameter.diameter)) {
                            diameterArray.add(diameter.diameter)
                        }
                    }
                }

                val diaAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, diameterArray)
                diaAd.setDropDownViewResource(R.layout.spinner_list_item)
                select_caller_diameter.adapter = diaAd
                select_caller_diameter.setSelection(0)

                select_caller_diameter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onNothingSelected(p0: AdapterView<*>?) {

                    }

                    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                        currentPointData.diameter = diameterArray[p2]
                    }
                }
            }
        }

        val materialAd = ArrayAdapter(activity!!, R.layout.spinner_list_item, typeNameArray)
        materialAd.setDropDownViewResource(R.layout.spinner_list_item)
        select_caller_material.adapter = materialAd
        select_caller_material.setSelection(0)
    }

    private var cameraPicker: CameraImagePicker? = null

    private fun capture() {
        cameraPicker = CameraImagePicker(this)
        cameraPicker!!.setDebugglable(true)
        cameraPicker!!.setImagePickerCallback(this)
        pickerPath = cameraPicker!!.pickImage()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Picker.PICK_IMAGE_CAMERA) {
                if (cameraPicker == null) {
                    cameraPicker = CameraImagePicker(this)
                    cameraPicker!!.setImagePickerCallback(this)
                    cameraPicker!!.reinitialize(pickerPath)
                }
                cameraPicker!!.submit(data)
            } else if (requestCode == REQUEST_VIDEO_CAPTURE) {
                val videoUri = Uri.parse(data?.dataString)
                val path = Util.getRealPathFromURI(activity!!.applicationContext, videoUri)
                Log.e("Video Path", path)
                currentImageData.id = currentPointData.id
                currentImageData.point_id = currentPointData.id
                currentImageData.file_path = path
                currentImageData.progress_type_id = "0"
            }
        }
    }

    override fun onImagesChosen(list: MutableList<ChosenImage>?) {
        Log.e("Image Picker", "Choosen")
        for (i in list!!.indices) {
            val m = list[i]
            val p = m.originalPath
            val currPath = getOutputMediaFileUri()
            Util.resizeImage(p, currPath.absolutePath)
            val currImagePath = currPath.absolutePath
            Log.e("Image Path", currImagePath)
            currentImageData.id = currentPointData.id
            currentImageData.point_id = currentPointData.id
            currentImageData.file_path = currImagePath
            currentImageData.progress_type_id = "0"
            preview.visibility = View.VISIBLE
            preview.setImageURI(Uri.fromFile(File(currImagePath)))
        }
    }

    private fun getOutputMediaFileUri(): File {
        var uploadDir = File(Environment.getExternalStorageDirectory().absolutePath + "/CPP")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        uploadDir = File(uploadDir, "images")
        if (!uploadDir.exists()) {
            uploadDir.mkdir()
        }
        return File(uploadDir, System.currentTimeMillis().toString() + ".jpg")
    }

    override fun onError(p0: String?) {

    }
    override fun onDestroyView() {
        super.onDestroyView()
        (activity as HomeActivity).cancelPointCreation()
    }
}