package org.horizen.ongoingprojectmonitoring.Main.Home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_choose_point_type.*
import org.horizen.ongoingprojectmonitoring.Models.Point
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootDialogFragment
import java.util.*

class ChooseSiteTypeDialog: RootDialogFragment() {

    companion object {

        @JvmStatic
        fun newInstance(lat: String, lon: String) = ChooseSiteTypeDialog().apply {
            val b = Bundle()
            b.putString("lat", lat)
            b.putString("lon", lon)
            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_choose_point_type, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

        val point = Point()
        point.id = UUID.randomUUID().toString()
        point.newPoint = true
        point.lat = arguments?.getString("lat") as String
        point.lon = arguments?.getString("lon") as String

        distFeature.setOnClickListener {
            val d = DistMainDialog.newInstance()
            d.show(activity!!.supportFragmentManager, "dist")
            dialog.dismiss()
        }

        risingFeature.setOnClickListener {
            val d = RisingMainDialog.newInstance()
            d.show(activity!!.supportFragmentManager, "dist")
            dialog.dismiss()
        }

        ohrFeature.setOnClickListener {
            val d = OhrPointDialog.newInstance(point)
            d.show(activity!!.supportFragmentManager, "ohr")
            dialog.dismiss()
        }

        wtpFeature.setOnClickListener {
            val d = WtpPointDialog.newInstance(point)
            d.show(activity!!.supportFragmentManager, "wtp")
            dialog.dismiss()
        }

        bsFeature.setOnClickListener {
            val d = BsPointDialog.newInstance(point)
            d.show(activity!!.supportFragmentManager, "bs")
            dialog.dismiss()
        }

        ipFeature.setOnClickListener {
            val d = IpPointDialog.newInstance(point)
            d.show(activity!!.supportFragmentManager, "ip")
            dialog.dismiss()
        }

        cancel.setOnClickListener {
            (activity as HomeActivity).cancelPointCreation()
            dialog.dismiss()
        }
    }


}