package org.horizen.ongoingprojectmonitoring.Main.Home

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_index.*
import org.horizen.ongoingprojectmonitoring.Helper.Defaults
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootDialogFragment

class IndexDialog: RootDialogFragment() {

    companion object {

        @JvmStatic
        fun newInstance() = IndexDialog().apply {  }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_index, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        show.setOnClickListener {
            (activity as HomeActivity).loadKmlTask()
            dialog.dismiss()
        }

        close.setOnClickListener {
            dialog.dismiss()
        }

        road.isChecked = prefs.getBoolean(Defaults.ROAD)
        scheme_boundary.isChecked = prefs.getBoolean(Defaults.PROJECT)
        block_boundary.isChecked = prefs.getBoolean(Defaults.BLOCK)
        gp_boundary.isChecked = prefs.getBoolean(Defaults.PANCHAYAT)
        mouza_boundary.isChecked = prefs.getBoolean(Defaults.MOUZA)
        zone_boundary.isChecked = prefs.getBoolean(Defaults.ZONE)
        settlement_boundary.isChecked = prefs.getBoolean(Defaults.SETTLEMENT)
        p_rmain.isChecked = prefs.getBoolean(Defaults.PRIMARY_RISING_MAIN)
        s_rmain.isChecked = prefs.getBoolean(Defaults.SECONDARY_RISING_MAIN)
        glr.isChecked = prefs.getBoolean(Defaults.GLR)
        cwr.isChecked = prefs.getBoolean(Defaults.CWR)
        eohr.isChecked = prefs.getBoolean(Defaults.EXISTING_OHR)
        pohr.isChecked = prefs.getBoolean(Defaults.PROPOSED_OHR)
        pip.isChecked = prefs.getBoolean(Defaults.PROPOSED_IP)
        eip.isChecked = prefs.getBoolean(Defaults.EXISTING_IP)
        eWtp.isChecked = prefs.getBoolean(Defaults.EXISTING_WTP)
        pWtp.isChecked = prefs.getBoolean(Defaults.PROPOSED_WTP)
        rbt.isChecked = prefs.getBoolean(Defaults.RBT)
        bs.isChecked = prefs.getBoolean(Defaults.BS)
        ph.isChecked = prefs.getBoolean(Defaults.PUMP_HOUSE)

        road.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.ROAD, b)
        }

        scheme_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PROJECT, b)
        }

        block_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.BLOCK, b)
        }

        gp_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PANCHAYAT, b)
        }

        mouza_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.MOUZA, b)
        }

        zone_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.ZONE, b)
        }

        settlement_boundary.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.SETTLEMENT, b)
        }

        p_rmain.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PRIMARY_RISING_MAIN, b)
        }

        s_rmain.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.SECONDARY_RISING_MAIN, b)
        }

        p_rmain.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PRIMARY_RISING_MAIN, b)
        }

        glr.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.GLR, b)
        }

        cwr.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.CWR, b)
        }

        eohr.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.EXISTING_OHR, b)
        }

        pohr.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PROPOSED_OHR, b)
        }

        bs.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.BS, b)
        }

        eWtp.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.EXISTING_WTP, b)
        }

        pWtp.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PROPOSED_WTP, b)
        }

        ph.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PUMP_HOUSE, b)
        }

        rbt.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.RBT, b)
        }

        eip.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.EXISTING_IP, b)
        }

        pip.setOnCheckedChangeListener { _, b ->
            prefs.putBoolean(Defaults.PROPOSED_IP, b)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as HomeActivity).cancelPointCreation()
    }
}