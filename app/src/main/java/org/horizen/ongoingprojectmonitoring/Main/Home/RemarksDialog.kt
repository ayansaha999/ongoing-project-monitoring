package org.horizen.ongoingprojectmonitoring.Main.Home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_remarks.*
import org.horizen.ongoingprojectmonitoring.Models.LocationPoint
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootDialogFragment
import android.graphics.*
import org.horizen.ongoingprojectmonitoring.Models.Point
import android.graphics.Bitmap
import android.text.StaticLayout
import android.graphics.Color.parseColor
import android.graphics.Paint.LINEAR_TEXT_FLAG
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.text.TextPaint
import org.horizen.ongoingprojectmonitoring.R.id.save
import android.graphics.Paint.LINEAR_TEXT_FLAG
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.graphics.Color.parseColor
import android.text.Layout
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.graphics.Color.LTGRAY
import android.net.Uri
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteImage
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteVideo
import org.jetbrains.anko.doAsync
import java.io.File


class RemarksDialog: RootDialogFragment() {
    companion object {
        @JvmStatic
        fun newInstance(image: ConstructionSiteImage?, video: ConstructionSiteVideo?, type: Int) = RemarksDialog().apply {
            val b = Bundle()
            b.putInt("type", type)
            b.putSerializable("image", image)
            b.putSerializable("video", video)
            arguments = b
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_remarks, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        dialog.setCanceledOnTouchOutside(false)
        dialog.setCancelable(false)

        val type = arguments?.getInt("type") as Int

        if (type == 0) {
            val locationPoint = arguments?.getSerializable("image") as ConstructionSiteImage

            imageView.visibility = View.VISIBLE
            imageView.setImageURI(Uri.fromFile(File(locationPoint.file_path)))

            cancel.setOnClickListener {
                (activity as HomeActivity).cancelPointCreation()
                dialog.dismiss()
            }

            save.setOnClickListener {
                val remark = remarks.text.toString()
                doAsync {
                    locationPoint.image_captionn = remark
                    locationPoint.status = false
                    (activity as HomeActivity).saveFeatureImage(locationPoint)
                }
                dialog.dismiss()
            }
        } else {
            val locationPoint = arguments?.getSerializable("video") as ConstructionSiteVideo
            videoView.visibility = View.VISIBLE
            videoView.setVideoURI(Uri.fromFile(File(locationPoint.file_path)))

            cancel.setOnClickListener {
                (activity as HomeActivity).cancelPointCreation()
                dialog.dismiss()
            }

            save.setOnClickListener {
                val remark = remarks.text.toString()
                locationPoint.image_caption = remark
                locationPoint.status = false
                (activity as HomeActivity).saveFeatureVideo(locationPoint)
                dialog.dismiss()
            }
        }
    }

    private fun getEncodedImage(path: String): Bitmap {
        val b = BitmapFactory.decodeFile(path)
        val workingBitmap = Bitmap.createBitmap(b)
        val mutableBitmap = workingBitmap.copy(Bitmap.Config.ARGB_8888, true)

        val bitmap = Bitmap.createBitmap(
                100, // Width
                50, // Height
                Bitmap.Config.ARGB_8888 // Config
        )

        val canvas = Canvas(bitmap)
        canvas.drawColor(Color.LTGRAY)
        val paint = Paint()
        paint.style = Paint.Style.FILL
        paint.color = Color.YELLOW
        paint.isAntiAlias = true

        val rectangle = Rect(
                10,
                10,
                canvas.width - 10,
                canvas.height - 10
        )

        canvas.drawRect(rectangle,paint)
//        val canvas = Canvas(mutableBitmap)

        paint.color = Color.WHITE
        paint.textSize = 5f
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER) as Xfermode?
        canvas.drawText("Ayan Saha", 10f, 10f, paint)

        return bitmap
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (activity as HomeActivity).cancelPointCreation()
    }
}
