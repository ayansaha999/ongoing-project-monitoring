package org.horizen.ongoingprojectmonitoring.Main.Home

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.content.Intent
import android.content.IntentSender
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.util.Log.e
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.Task
import com.google.maps.android.data.kml.KmlLayer
import kotlinx.android.synthetic.main.content_home.*
import org.horizen.ongoingprojectmonitoring.Helper.DataTypeConverter
import org.horizen.ongoingprojectmonitoring.Helper.Defaults
import org.horizen.ongoingprojectmonitoring.Helper.Server
import org.horizen.ongoingprojectmonitoring.Main.Auth.AuthActivity
import org.horizen.ongoingprojectmonitoring.Main.Report.ReportActivity
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteImage
import org.horizen.ongoingprojectmonitoring.Models.ConstructionSiteVideo
import org.horizen.ongoingprojectmonitoring.Models.LocationPoint
import org.horizen.ongoingprojectmonitoring.Models.Point
import org.horizen.ongoingprojectmonitoring.R
import org.horizen.ongoingprojectmonitoring.Root.RootActivity
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.xmlpull.v1.XmlPullParserException
import java.io.ByteArrayInputStream
import java.io.IOException

class HomeActivity : RootActivity(), OnMapReadyCallback {

    private var pointList: List<Point>? = null

    private var markerCreationActive = false
    private var markerCreated = false

    private var locationPointMarkerOptions = ArrayList<MarkerOptions>()

    private var latLng: LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        val builder = LocationSettingsRequest.Builder()
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener { _ ->

        }

        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult?) {
                locationResult ?: return
                for (location in locationResult.locations){

                }
            }
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    exception.startResolutionForResult(this@HomeActivity, 1000)
                } catch (sendEx: IntentSender.SendIntentException) {

                }
            }
        }

        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        myLocation.setOnClickListener {
            getLastLocation()
        }

        zoomIn.setOnClickListener {
            zoomIn()
        }

        zoomOut.setOnClickListener {
            zoomOut()
        }

        add.setOnClickListener {
            markerCreationActive = true
            add.hide()
            rootVm.actionMessage.value = "Click on map to create point and wait until it is visible!"
        }

        done.setOnClickListener {
            done.hide()
            markerCreated = false
            val dialog = ChooseSiteTypeDialog.newInstance(latLng!!.latitude.toString(), latLng!!.longitude.toString())
            dialog.show(supportFragmentManager, "choose")
        }

        index.setOnClickListener {
            val dialog = IndexDialog.newInstance()
            dialog.show(supportFragmentManager, "index")
        }

        report.setOnClickListener {
            startActivity(Intent(this@HomeActivity, ReportActivity::class.java))
        }

        logout.setOnClickListener {
            prefs.putBoolean(Defaults.LOGGED_IN, false)
            startActivity(Intent(this@HomeActivity, AuthActivity::class.java))
        }

        rootVm.actionMessage.observe(this, Observer { actionMessageVal ->
            actionText.text = actionMessageVal
        })
    }

    override fun onResume() {
        super.onResume()
        startLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private fun startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null /* Looper */)
    }

    override fun onPause() {
        super.onPause()
        stopLocationUpdates()
    }

    private fun stopLocationUpdates() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    private var activeMarker: Marker? = null

    @SuppressLint("MissingPermission")
    override fun onMapReady(map: GoogleMap?) {
        mMap = map!!
        mMap!!.mapType = GoogleMap.MAP_TYPE_HYBRID

        loadKmlTask()

        mMap!!.setOnMarkerClickListener { marker ->
            checkPoint(marker.position)
            return@setOnMarkerClickListener false
        }

        mMap!!.setOnMapClickListener { latlng ->
            latLng = latlng
            if (markerCreationActive) {
                markerCreationActive = false
                markerCreated = true
                activeMarker = mMap!!.addMarker(MarkerOptions().draggable(true).position(latlng).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
                rootVm.actionMessage.value = "Drag the marker to get accurate location details and click on ADD DETAILS icon to enter point details!"
                done.show()
            }
        }

        mMap!!.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragEnd(p0: Marker?) {
                latLng = p0?.position
            }

            override fun onMarkerDragStart(p0: Marker?) {
                latLng = p0?.position
            }

            override fun onMarkerDrag(p0: Marker?) {
                latLng = p0?.position
            }
        })

        mMap!!.uiSettings.isMyLocationButtonEnabled = false
        mMap!!.isMyLocationEnabled = true

        rootVm.locationPointList?.observe(this, Observer { locationPointList ->
            if (locationPointList != null && locationPointList.isNotEmpty()) {
                for (locationPoint in locationPointList) {
                    val m = MarkerOptions().position(LatLng(locationPoint.gps_lat.toDouble(), locationPoint.gps_lon.toDouble())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_node))
                    if (!locationPointMarkerOptions.contains(m)) {
                        locationPointMarkerOptions.add(m)
                        mMap!!.addMarker(m)
                    }
                }
            }
        })

        rootVm.imagesList?.observe(this, Observer { locationPointList ->
            if (locationPointList != null && locationPointList.isNotEmpty()) {
                for (locationPoint in locationPointList) {
                    val m = MarkerOptions().position(LatLng(locationPoint.gps_lat.toDouble(), locationPoint.gps_lon.toDouble())).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_node))
                    if (!locationPointMarkerOptions.contains(m)) {
                        locationPointMarkerOptions.add(m)
                        mMap!!.addMarker(m)
                    }
                }
            }
        })
    }

    private fun zoomIn() {
        mMap!!.animateCamera(CameraUpdateFactory.zoomIn())
    }

    private fun zoomOut() {
        mMap!!.animateCamera(CameraUpdateFactory.zoomOut())
    }

    fun loadKmlTask() {
        if (mMap != null) {
            rootVm.actionMessage.value = "Please wait until the map is loaded."
            mMap!!.clear()
            doAsync {
                val user = localDb?.userDao()?.findLoggedInUser(true)
                if (user != null) {
                    e("Loading User Scheme Id", user.project_id)
                    val kml = localDb?.kmlDao()?.findKmlById(user.project_id)
                    if (kml != null) {
                        runOnUiThread {
                            rootVm.activeUser.value = user
                            project_name.text = kml.project_name
                        }
                        e("Loading Kml Scheme Id",  "${kml.project_id} - ${kml.project_name}")
                        if (prefs.getBoolean(Defaults.PROJECT)) {
                            val kmlPath = "$mainPath/${kml.project_id}/scheme.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.BLOCK)) {
                            val kmlPath = "$mainPath/${kml.project_id}/block.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.PANCHAYAT)) {
                            val kmlPath = "$mainPath/${kml.project_id}/panchayat.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.MOUZA)) {
                            val kmlPath = "$mainPath/${kml.project_id}/mouza.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.ZONE)) {
                            val kmlPath = "$mainPath/${kml.project_id}/zone.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.SETTLEMENT)) {
                            val kmlPath = "$mainPath/${kml.project_id}/settlement.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.ROAD)) {
                            val kmlPath = "$mainPath/${kml.project_id}/road.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.WATER_BODY)) {
                            val kmlPath = "$mainPath/${kml.project_id}/water_body.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.SECONDARY_RISING_MAIN)) {
                            val kmlPath = "$mainPath/${kml.project_id}/secondary_rising_main.kml"
                            loadKmlLayer(kmlPath)
                        }
                        if (prefs.getBoolean(Defaults.PRIMARY_RISING_MAIN)) {
                            val kmlPath = "$mainPath/${kml.project_id}/primary_rising_main.kml"
                            loadKmlLayer(kmlPath)
                        }


                        runOnUiThread {
                            val cameraUpdate = CameraUpdateFactory.newCameraPosition(CameraPosition.fromLatLngZoom(LatLng(kml.center_lat.toDouble(), kml.center_lon.toDouble()), 10f))
                            mMap!!.animateCamera(cameraUpdate)
                            rootVm.actionMessage.value = "Map loaded successfully. Click on the ADD icon to create new point."

                        }

                        pointList = localDb?.pointDao()?.findPointByScheme(user.project_id)
                        if (pointList != null && pointList!!.isNotEmpty()) {
                            e("Point Data", pointList?.size.toString())
                            for (point in pointList!!) {
                                loadPointData(point)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun loadKmlLayer(data: String) {
        Log.e("boundary_path", data)
        val dirExists = storage.isFileExist(data)
        if (dirExists) {
            val bytes = storage.readFile(data)
            try {
                runOnUiThread {
                    val kmlBoundary = KmlLayer(mMap, ByteArrayInputStream(bytes),
                            applicationContext)
                    kmlBoundary.addLayerToMap()
                }
            } catch (e: XmlPullParserException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    private fun loadPointData(point: Point) {
        runOnUiThread {
            val l = LatLng(point.lat.toDouble(), point.lon.toDouble())
            val title = point.location
            if (prefs.getBoolean(Defaults.EXISTING_WTP)) {
                if (point.location_type.equals("5")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_existing_wtp)))
                }
            }
            if (prefs.getBoolean(Defaults.PROPOSED_WTP)) {
                if (point.location_type.equals("2")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_proposed_wtp)))
                }
            }
            if (prefs.getBoolean(Defaults.BS)) {
                if (point.location_type.equals("3")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_boosting_system)))
                }
            }
            if (prefs.getBoolean(Defaults.EXISTING_OHR)) {
                if (point.location_type.equals("4")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_existing_ohr)))
                }
            }
            if (prefs.getBoolean(Defaults.PROPOSED_OHR)) {
                if (point.location_type.equals("6")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_proposed_ohr)))
                }
            }
            if (prefs.getBoolean(Defaults.EXISTING_IP)) {
                if (point.location_type.equals("1")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_intake_point)))
                }
            }
            if (prefs.getBoolean(Defaults.PROPOSED_IP)) {
                if (point.location_type.equals("8")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_proposed_intake)))
                }
            }
            if (prefs.getBoolean(Defaults.GLR)) {
                if (point.location_type.equals("7")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cwr)))
                }
            }
            if (prefs.getBoolean(Defaults.PUMP_HOUSE)) {
                if (point.location_type.equals("9")) {
                    mMap!!.addMarker(MarkerOptions().position(l).title(title).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_pump_house)))
                }
            }
        }
    }

    private fun checkPoint(position: LatLng) {
        for (point in pointList!!) {
            val latLng =  LatLng(point.lat.toDouble(), point.lon.toDouble())
            if (latLng == position) {
                if (prefs.getBoolean(Defaults.EXISTING_WTP)) {
                    if (point.location_type.equals("5")) {
                        val dialog = WtpPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.PROPOSED_WTP)) {
                    if (point.location_type.equals("2")) {
                        val dialog = WtpPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.BS)) {
                    if (point.location_type.equals("3")) {
                        val dialog = BsPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.EXISTING_OHR)) {
                    if (point.location_type.equals("4")) {
                        val dialog = OhrPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.PROPOSED_OHR)) {
                    if (point.location_type.equals("6")) {
                        val dialog = OhrPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.EXISTING_IP)) {
                    if (point.location_type.equals("1")) {
                        val dialog = IpPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.PROPOSED_IP)) {
                    if (point.location_type.equals("8")) {
                        val dialog = IpPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
                if (prefs.getBoolean(Defaults.GLR)) {
                    if (point.location_type.equals("7")) {
                        val dialog = BsPointDialog.newInstance(point)
                        dialog.show(supportFragmentManager, point.location_type)
                    }
                }
            }
        }
    }

    fun cancelPointCreation() {
        add.show()
        rootVm.actionMessage.value = "Click on the ADD icon to create new point."
        markerCreationActive = false
        markerCreated = false
        if (activeMarker != null) {
            activeMarker!!.remove()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }

    fun saveLocationPoint(currentPointData: LocationPoint, currentImageData: ConstructionSiteImage) {
        currentPointData.gps_lat = latLng?.latitude.toString()
        currentPointData.gps_lon = latLng?.longitude.toString()
        currentPointData.project_id = rootVm.activeUser.value?.project_id!!
        currentPointData.session_key = "81dc9bdb52d04dc20036dbd8313ed055"
        currentPointData.userid = prefs.getString("user_id")

        currentImageData.status = true
        currentImageData.gps_lat = currentPointData.gps_lat
        currentImageData.gps_lon = currentPointData.gps_lon
        currentImageData.user_id = prefs.getString("user_id")

        doAsync {
            localDb?.locationPointDao()?.insertLocationPoint(currentPointData)
            localDb?.constructionSiteImageDao()?.insertConstructionSiteImage(currentImageData)
        }
    }

    fun showRemarksDialog(image: ConstructionSiteImage?, video: ConstructionSiteVideo?, type: Int) {
        val dialog = RemarksDialog.newInstance(image, video, type)
        dialog.show(supportFragmentManager, "remark")
    }
}
